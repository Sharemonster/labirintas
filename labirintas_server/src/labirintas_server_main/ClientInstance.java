package labirintas_server_main;

import java.net.InetAddress;
import java.net.Socket;

public class ClientInstance{
	public final InetAddress ip;
	public final int port;
	private Socket matchKey;
	public ClientInstance(InetAddress ip, int port){
		this.ip=ip;
		this.port=port;
	}
	@Override public String toString(){ return ip.toString()+":"+port; }
	
	public void setMatchKey(Socket matchKey) {
		this.matchKey = matchKey;
	}
	
	public Socket getMatchKey() {
		return matchKey;
	}
}
