package labirintas_server_main;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;

import labirintas_message.*;

public class Server {
	private int port;
	private boolean serverOpen = true;
	private ServerSocket serverSocket;
	volatile private ArrayList<Socket> clientList = new ArrayList<>();
	volatile private Hashtable<String, Match> matchID_match_Table = new Hashtable<>();
	volatile private ArrayList<Skelbimas> advertList = new ArrayList<>();

	public Server(int port) {
		try {
			serverSocket = new ServerSocket(port);
			if (this.port == 0)
				this.port = serverSocket.getLocalPort();
			else
				this.port = port;
			Thread serverThread = new Thread(new Runnable() {
				public void run() {
					AdvertChecker advertChecker = new AdvertChecker(advertList, matchID_match_Table);
					new Thread(advertChecker).start();
					while (serverOpen) {
						try {
							final Socket clientSocket = serverSocket.accept();

							ClientListener clientListener = new ClientListener(clientList, clientSocket, serverOpen,
									matchID_match_Table, advertList);
							Thread clientThread = new Thread(clientListener);
							clientThread.setDaemon(true);
							clientThread.setName("Client " + clientSocket.getInetAddress().toString());
							clientThread.start();

						} catch (SocketException e) {
						} catch (IOException e) {
							e.printStackTrace();
						}

					}

				}

			});
			serverThread.setName("Server");
			serverThread.start();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void dispose() {
		serverOpen = false;
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Socket s : clientList) {
			try {
				s.close();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		clientList.clear();
		clientList = null;
		serverSocket = null;
		serverClosed();
	}

	public String getIp() {
		try {
			serverSocket.getInetAddress();
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void kickClient(ClientInstance client) {
		Socket s;
		for (int i = 0; i < clientList.size(); i++) {
			s = clientList.get(i);
			if (client.ip == s.getInetAddress() && s.getPort() == client.port) {
				try {
					s.shutdownOutput();
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return;
			}
		}
	}

	public void recievedInput(ClientInstance client, String msg) {
		System.out.println(client.toString() + " wrote " + msg);
	}

	public void serverClosed() {
		System.out.println("Server closed");
	}
}
