package labirintas_server_main;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import labirintas_message.Message;
import labyrinth_game.Controller;
import labyrinth_game.Control.Movement;
import labyrinth_game.Game.GameMap;
import labyrinth_game.Game.HumanPlayer;
import labyrinth_game.Game.MoveResult;
import labyrinth_game.Game.Player;

public class Match {
	GameMap gameMap;
	LinkedHashMap<Socket, Player> playerList;
	ArrayList<ObjectOutputStream> messageRecipients;
	Controller controlObject;
	int playersTurnIndex = 0, maxPlayerCount;
	boolean gameOver = false;

	public Match(String message, Socket playerSocket, ObjectOutputStream outputStream) {
		String[] tokens = message.split(" ");
		try {
			gameMap = new GameMap(Integer.parseInt(tokens[0]));
			maxPlayerCount = Integer.parseInt(tokens[1]);
			playerList = new LinkedHashMap<Socket, Player>(maxPlayerCount);
			playerList.put(playerSocket, new HumanPlayer());
			playerList.get(playerSocket).setName(playerSocket.getInetAddress().toString());
			messageRecipients = new ArrayList<ObjectOutputStream>();
			messageRecipients.add(outputStream);
			controlObject = new Controller(gameMap);
		} catch (Exception e) {
			System.out.println(e.getMessage() + " || " + playerSocket.toString());
		}
	}

	public Message addPlayer(Socket zaidejas, ObjectOutputStream send) {
		Object result = playerList.put(zaidejas, new HumanPlayer());
		if (result != null) {
			return new Message(Message.SYSTEM_MSG, 3, "", null);
		} else {
			messageRecipients.add(send);
			playerList.get(zaidejas).setName(zaidejas.getInetAddress().toString());
			return new Message(Message.SYSTEM_MSG, 2, "", null);
		}
	}

	public void doMove(Socket zaidejas, String msg) {
		String[] command = msg.split(" ");
		MoveResult moveResult = null;
		Player[] players = new Player[playerList.size()];
		for (int i = 0; i < playerList.size(); i++) {
			players[i] = new ArrayList<Player>(playerList.values()).get(i);
		}
		System.out.println(new ArrayList<Player>(playerList.values()).get(playersTurnIndex));
		if (playerList.get(zaidejas).equals(new ArrayList<Player>(playerList.values()).get(playersTurnIndex))
				&& !playerList.get(zaidejas).checkDead() && !gameOver) {
			switch (Integer.parseInt(command[0]) - 11) {
			case 0:
			case 1:
			case 2:
			case 3: {
				moveResult = controlObject.executeMove(Integer.parseInt(command[0]) - 11, 0, playerList.get(zaidejas),
						players);
				break;
			}
			case 4:
			case 5:
			case 6: {

				moveResult = controlObject.executeMove(Integer.parseInt(command[1]),
						command.length == 3 ? Integer.parseInt(command[2]) : 0, players[playersTurnIndex], players);
				broadcastMessages(new Message(Message.GAME_MSG, 11, moveResult.getMessage() + "\n", null));
				break;
			}
			}

			processResult(zaidejas, moveResult);
		}

	}

	private void processResult(Socket zaidejas, MoveResult moveResult) {
		if (moveResult.getResult() == 2) { // jei zaidimas baigtas
			broadcastMessages(new Message(Message.GAME_MSG, 10, moveResult.getMessage(), null));
			gameOver = true;
		} else if (moveResult.getResult() == 0) { // jei nebuvo padarytas ejimas
			broadcastMessages(new Message(Message.GAME_MSG, 11, moveResult.getMessage(), null));
		} else { // jei ejimas sekmingas
			ArrayList<String> s = new ArrayList<String>();
			moveResult.setResult(Movement.executeCell(gameMap.getGameMap(), playerList.get(zaidejas), s));
			for (String a : s) {
				moveResult.appendMessage(a);
			}

			String nextPlayerName = getNextPlayer();

			broadcastMessages(
					new Message(Message.GAME_MSG, 11, moveResult.getMessage() + nextPlayerName + " turn\n", null));
		}
	}

	private String getNextPlayer() {
		String nextPlayerName;
		if (playersTurnIndex + 1 < playerList.size())
			playersTurnIndex++;
		else
			playersTurnIndex = 0;
		while (new ArrayList<Player>(playerList.values()).get(playersTurnIndex).getDead()) {
			if (playersTurnIndex + 1 < playerList.size())
				playersTurnIndex++;
			else
				playersTurnIndex = 0;
		}
		nextPlayerName = new ArrayList<Player>(playerList.values()).get(playersTurnIndex).getName();
		return nextPlayerName;
	}

	public int getPlayerCount() {
		return playerList.size();
	}

	public Message getMap() {
		return new Message(Message.SYSTEM_MSG, 9, gameMap.printGameMap(), null);
	}

	public int getMaxPlayerCount() {
		return maxPlayerCount;
	}

	public int getMapSize() {
		return gameMap.getMapSize();
	}

	public void removePlayer(Socket s) {
		playerList.remove(s);
	}

	private void broadcastMessages(Message msg) {
		for (int i = 0; i < messageRecipients.size(); i++) {
			System.out.println(messageRecipients.size());
			System.out.println(messageRecipients.toString());
			try {
				messageRecipients.get(i).writeObject(msg);
				System.out.println("Message Sent");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error!");
			}
		}
	}
}
