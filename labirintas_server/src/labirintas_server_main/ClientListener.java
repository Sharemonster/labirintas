package labirintas_server_main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

import labirintas_message.Message;
import labirintas_message.Skelbimas;

public class ClientListener implements Runnable {

	ArrayList<Socket> clientList;
	Socket clientSocket;
	boolean serverOpen;
	Hashtable<String, Match> matchID_match_Table;
	volatile ArrayList<Skelbimas> advertList;

	public ClientListener(ArrayList<Socket> clientList, Socket clientSocket, boolean serverOpen,
			Hashtable<String, Match> matchID_match_Table, ArrayList<Skelbimas> advertList) {
		this.clientList = clientList;
		this.clientSocket = clientSocket;
		this.serverOpen = serverOpen;
		this.matchID_match_Table = matchID_match_Table;
		this.advertList = advertList;
	}

	public void run() {
		try {
			clientList.add(clientSocket);

			ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
			String gameID = "";

			ClientInstance client = new ClientInstance(clientSocket.getInetAddress(), clientSocket.getPort());
			clientConncted(client);

			while (serverOpen) {
				try {
					Message msg = (Message) in.readObject();

					if (!gameID.equals("")) {
						executeClientCommand(msg, clientSocket, out, matchID_match_Table.get(gameID), gameID);
					} else {
						gameID = executeClientCommand(msg, clientSocket, out, null, gameID);
					}

				} catch (IOException e) {
					clientDisconnected(client);
					try {
						if (!clientSocket.isClosed()) {
							if (matchID_match_Table.get(gameID) != null)
								matchID_match_Table.get(gameID).removePlayer(clientSocket);
							clientSocket.shutdownOutput();
							clientSocket.close();
						}
					} catch (Exception exception) {
						exception.printStackTrace();
					}
					clientList.remove(clientSocket);
					return;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		try {
			clientSocket.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		clientList.remove(clientSocket);
	}

	public String executeClientCommand(Message msg, Socket clientsock, ObjectOutputStream out, Match match,
			String gameID) {
		switch (msg.getTipas()) {
		case Message.LOGIN: {
			break;
		}
		case Message.SYSTEM_MSG: {
			switch (msg.getKomandosTipas()) {
			case 1: { // Sukurti partija, skelbima
				if (gameID.equals("")) {
					gameID = UUID.randomUUID().toString();
					matchID_match_Table.put(gameID, new Match(msg.getMsg(), clientsock, out));
					String[] tokens = msg.getMsg().split(" ");
					advertList.add(new Skelbimas(gameID, Integer.parseInt(tokens[0]),
							matchID_match_Table.get(gameID).getPlayerCount(), Integer.parseInt(tokens[1])));
					sendMessage(out, new Message(Message.SYSTEM_MSG, 1,
							Integer.toString(matchID_match_Table.get(gameID).getMapSize()), null));
				} else
					sendMessage(out, new Message(Message.SYSTEM_MSG, 2, "", null));
				break;
			}
			case 2: { // Prisijungti prie partijos

				gameID = msg.getMsg();
				if (matchID_match_Table.get(gameID).getPlayerCount() < matchID_match_Table.get(gameID)
						.getMaxPlayerCount()) {
					System.out.println(matchID_match_Table.get(gameID).getPlayerCount());
					matchID_match_Table.get(gameID).addPlayer(clientsock, out);
					sendMessage(out, new Message(Message.SYSTEM_MSG, 3,
							Integer.toString(matchID_match_Table.get(gameID).getMapSize()), null));
				} else {
					gameID = "";
					sendMessage(out, new Message(Message.SYSTEM_MSG, 4, "", null));
				}
				break;

			}
			case 8: { // gauti skelbimus
				boolean sent = false;
				while (!sent) {
					if (!advertList.isEmpty()) {
						sendMessage(out, new Message(Message.SYSTEM_MSG, 8, "", advertList));
					}
					sent = true;

				}
				break;
			}
			case 9: { // gauti labirinta
				if (match == null)
					sendMessage(out, new Message(Message.SYSTEM_MSG, 0, "", null));
				else
					sendMessage(out, match.getMap());
				break;
			}
			}
			break;
		}
		case Message.GAME_MSG: {
			if (match == null) {// eiti, naudoti ginkla
				sendMessage(out, new Message(Message.SYSTEM_MSG, 0, "", null));
			} else {
				System.out.println(msg.getKomandosTipas() + " " + msg.getMsg());
				match.doMove(clientsock, msg.getKomandosTipas() + " " + msg.getMsg());
			}
			break;
		}
		case Message.LOGOUT: {
			break;
		}
		}
		return gameID;

	}

	public void sendMessage(ObjectOutputStream out, Message msg) {
		try {
			out.writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void clientConncted(ClientInstance client) {
		System.out.println("Client " + client.toString() + " Connected");
	}

	public void clientDisconnected(ClientInstance client) {
		System.out.println("Client " + client.toString() + " Disconnected");
	}
}
