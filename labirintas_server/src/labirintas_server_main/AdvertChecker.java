package labirintas_server_main;

import java.util.ArrayList;
import java.util.Hashtable;

import labirintas_message.Skelbimas;

public class AdvertChecker implements Runnable {

	private static final long SLEEP_TIME = 1000;
	
	volatile ArrayList<Skelbimas> notices;
	volatile Hashtable<String, Match> match;

	public AdvertChecker(ArrayList<Skelbimas> notices, Hashtable<String, Match> match) {
		this.notices = notices;
		this.match = match;
	}

	@Override
	public void run() {
		while (true) {
			printNotices();
			sleepThread();
		}

	}
	
	private void printNotices() {
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		for (int i = 0; i < notices.size(); i++) {
			printNotice(notices.get(i));
			notices.get(i).setZaidejuSkaicius(match.get(notices.get(i).getGameID()).getPlayerCount());
			if (notices.get(i).getZaidejuSkaicius() >= notices.get(i).getMaxZaidejuSkaicius()
					|| notices.get(i).getZaidejuSkaicius() <= 0) {
				notices.remove(i);
			}
		}
	}
	
	private void printNotice(Skelbimas notice) {
		System.out.println(notice.getGameID() + " " + notice.getDydis() + " "
				+ notice.getZaidejuSkaicius() + "/" + notice.getMaxZaidejuSkaicius());
		
	}
	
	private void sleepThread() {
		try {
			Thread.sleep(SLEEP_TIME);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
