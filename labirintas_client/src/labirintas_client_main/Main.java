package labirintas_client_main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
	static String serverName = "127.0.0.1", userID = "defaultName";
	static int port = 48840;

	public static void main(String[] args) {

		getProperties();

		new Client(serverName, port);

	}

	private static void getProperties() {
		File properties = new File("properties.txt");

		try {
			Scanner sf = new Scanner(properties);
			serverName = sf.nextLine();
			port = Integer.parseInt(sf.nextLine());
			sf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
