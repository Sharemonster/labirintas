package labirintas_client_main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import labirintas_message.*;

public class Client {

	

	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private boolean open = true;
	Message msg = null;
	String ip;
	int port, labirinto_dydis = 0;
	String[][] skelbimai = new String[100][3];
	ArrayList<Skelbimas> skelbimaiList;
	Timer time = new Timer("Skelbimai", true);

	public Client(String ip, int port, GUI gui) {
		this.ip = ip;
		this.port = port;
		gui.createGUI();
		try {
			socket = new Socket(ip, port);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			time.scheduleAtFixedRate(new TimerTask() {
				public void run() {
					send(new Message(Message.SYSTEM_MSG, 8, "", null));
				}
			}, 0, 5000);
			Thread clientThread = new Thread(new Runnable() {
				public void run() {
					while (open) {
						try {
							if (msg != null) {
								if (msg.getMsg().equals(null)) {
									open = false;
									disconnected();
									try {
										if (socket != null)
											socket.close();
									} catch (Exception exception) {
										exception.printStackTrace();
									}
									try {
										if (in != null)
											in.close();
									} catch (Exception exception) {
										exception.printStackTrace();
									}
									try {
										if (out != null)
											out.close();
									} catch (Exception exception) {
										exception.printStackTrace();
									}
									return;
								}
							}

							Message msg = (Message) in.readObject();
							recievedInput(msg);

						} catch (IOException exception) {
							open = false;
							serverClosed();
							try {
								socket.close();
							} catch (Exception exception1) {
								exception.printStackTrace();
							}
							try {
								in.close();
							} catch (Exception exception1) {
								exception.printStackTrace();
							}
							try {
								out.close();
							} catch (Exception exception1) {
								exception.printStackTrace();
							}
							return;
						} catch (Exception exception) {
							exception.printStackTrace();
						}

					}
				}
			});
			clientThread.setName("Rysys su serveriu");
			clientThread.start();
			connectedToServer();
		} catch (UnknownHostException exception) {
			open = false;
			unknownHost();
		} catch (IOException exception) {
			open = false;
			couldNotConnect();
		} catch (Exception exception) {
			open = false;
			exception.printStackTrace();
		}
	}

	public void dispose() {
		try {
			if (open) {
				open = false;
				socket.close();
				in.close();
				out.close();
				disconnected();
			}
			socket = null;
			in = null;
			out = null;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void send(Message msg) {
		if (open)
			try {
				out.writeObject(msg);
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public boolean isConnected() {
		return open;
	}

	public void siustiKomanda(String msg) {
		msg.toLowerCase();
		String[] tokens = msg.split(" ");
		switch (tokens[0]) {
		case "a": {

		}
		case "aukstyn": {
			send(new Message(Message.GAME_MSG, 11, "", null));
			break;
		}
		case "d": {

		}
		case "desinen": {
			send(new Message(Message.GAME_MSG, 12, "", null));
			break;
		}
		case "z": {

		}
		case "zemyn": {
			send(new Message(Message.GAME_MSG, 13, "", null));
			break;
		}
		case "k": {

		}
		case "kairen": {
			send(new Message(Message.GAME_MSG, 14, "", null));
			break;
		}
		case "spausdinti": {
			send(new Message(Message.SYSTEM_MSG, 9, "", null));
			break;
		}
		case "sauti": {

		}
		case "s": {
			send(new Message(Message.GAME_MSG, 17, "1 " + getDirection(tokens[1]), null));
			break;
		}
		case "granata": {

		}
		case "g": {
			send(new Message(Message.GAME_MSG, 17, "2 " + getDirection(tokens[1]), null));
			break;
		}
		case "durti": {

		}
		case "p": {
			send(new Message(Message.GAME_MSG, 17, "0", null));
			break;
		}
		}
	}

	public void pakeistiSkelbimus() {
		frame.remove(gameboard);
		frame.add(instructions, BorderLayout.LINE_START);
	}

	public void pakeistiInstrukcijas() {
		frame.remove(instructions);
		frame.add(gameboard, BorderLayout.LINE_START);
	}

	public void prisijungtiPrieZaidimo() {
		try {
			send(new Message(Message.SYSTEM_MSG, 2, skelbimaiList.get(lentele.getSelectedRow()).getGameID().toString(),
					null));
		} catch (Exception e) {
			System.out.println("Pasirinkite partija prie kurios norite prisijungti");
		}
	}

	public void unknownHost() {
		System.out.println("Nežinomas serveris");
	}

	public void couldNotConnect() {
		System.out.println("Nepavyko prisijungti");
	}

	public void recievedInput(Message msg) {
		String[] tokens = msg.getMsg().split(" ");
		switch (msg.getTipas()) {
		case Message.LOGIN: {
			break;
		}
		case Message.SYSTEM_MSG: {
			switch (msg.getKomandosTipas()) {
			case 0: {
				System.out.println("Neprisijungta prie zaidimo");
				break;
			}
			case 1: {
				System.out.println("Zaidimas sukurtas");
				System.out.println(
						"Vaikscioti:\naukstyn | a\ndesinen | d\nzemyn | z\nkairen | k\nGinklai:\nPeilis: durti | p\nKulkos: sauti + kryptis| s + kryptis\nPvz. sauti z\nGranatos: granata + kryptis | g + kryptis\nPvz. g d\n");
				labirinto_dydis = Integer.parseInt(msg.getMsg());
				pakeistiSkelbimus();
				break;
			}
			case 2: {
				System.out.println("Jau esate zaidime");
				break;
			}
			case 3: {
				System.out.println("Prisijungete prie zaidimo");
				System.out.println(
						"Vaikscioti:\naukstyn | a\ndesinen | d\nzemyn | z\nkairen | k\nGinklai:\nPeilis: durti | p\nKulkos: sauti + kryptis| s + kryptis\nPvz. sauti z\nGranatos: granata + kryptis | g + kryptis\nPvz. g d\n");
				labirinto_dydis = Integer.parseInt(msg.getMsg());
				pakeistiSkelbimus();
				break;
			}
			case 4: {
				System.out.println("Prisijungti prie zaidimo nepavyko");
				break;
			}
			case 8: {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						skelbimaiList = msg.getSkelbimai();
						skelbimai = new String[skelbimaiList.size()][3];
						lentelesmodelis.setRowCount(0);
						// System.out.println(
						// skelbimaiList.size() + " || " + skelbimai.length + " || " +
						// msg.getSkelbimai().size());
						for (int i = 0; i < skelbimai.length; i++) {
							skelbimai[i][0] = Integer.toString(skelbimaiList.get(i).getDydis());
							skelbimai[i][1] = Integer.toString(skelbimaiList.get(i).getZaidejuSkaicius());
							skelbimai[i][2] = Integer.toString(skelbimaiList.get(i).getMaxZaidejuSkaicius());
							lentelesmodelis.addRow(skelbimai[i]);
							// System.out.println(lentelesmodelis.getRowCount());
						}
						lentele.setModel(lentelesmodelis);
						lentelesmodelis.fireTableDataChanged();
						lentele.repaint();

					}

				});

				break;
			}
			case 9: {
				System.out.println(msg.getMsg());
				break;
			}
			}
		}
		case Message.GAME_MSG: {
			switch (msg.getKomandosTipas()) {
			case 10:
			case 11: {
				System.out.println(msg.getMsg());
				break;
			}
			}
		}
		case Message.LOGOUT: {
			break;
		}
		}
	}

	public int getDirection(String direction) {
		int kryptis = -1;
		switch (direction) {
		case "k": {

		}
		case "kairen": {

		}
		case "3": {
			kryptis = 3;
			break;
		}
		case "a": {

		}
		case "aukstyn": {

		}
		case "0": {
			kryptis = 0;
			break;
		}
		case "d": {

		}
		case "desinen": {

		}
		case "1": {
			kryptis = 1;
			break;
		}
		case "z": {

		}
		case "zemyn": {

		}
		case "2": {
			kryptis = 2;
			break;
		}
		}
		return kryptis;
	}

	public void serverClosed() {
		System.out.println("Serveris uždarytas");
	}

	public void disconnected() {
		System.out.println("Atsijungta");
	}

	public void connectedToServer() {
		System.out.println("Prisijungta");
	}





	
}