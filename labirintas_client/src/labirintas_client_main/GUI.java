package labirintas_client_main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import labirintas_message.Message;

public class GUI {
	private JFrame frame;

	private JPanel buttonpanel;
	private JPanel consolepanel;
	private JPanel noticeboard;
	private JPanel gameboard;
	private JPanel instructions;

	private JButton pradetizaidima;
	private JButton sukurtizaidima;
	private JButton zaidimo_nustatymai;
	private JButton prisijungti;

	private JTextArea ivestis;
	private JButton ivesti;

	private JTable lentele;
	private DefaultTableModel lentelesmodelis;
	private JScrollPane lentelescroll;

	private JLabel instrukcijos;

	private JTextArea txt;
	private JScrollPane scroll;
	private PrintStream con;

	public void createGUI() {
		frame = new JFrame();
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Labirintas");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());

		buttonpanel = new JPanel();
		buttonpanel.setLayout(new BoxLayout(buttonpanel, BoxLayout.PAGE_AXIS));
		consolepanel = new JPanel();
		consolepanel.setLayout(new BorderLayout());

		instructions = new JPanel();
		instrukcijos = new JLabel(
				"Vaikscioti:\naukstyn | a\ndesinen | d\nzemyn | z\nkairen | k\nGinklai:\nPeilis: durti | p\nKulkos: sauti + kryptis| s + kryptis\nPvz. sauti z\nGranatos: granata + kryptis | g + kryptis\nPvz. g d\n");

		gameboard = new JPanel();
		gameboard.setLayout(new GridBagLayout());

		lentele = new JTable(skelbimai, new String[] { "Labirinto dydis", "Prisijunge", "Max zaideju" });
		lentelesmodelis = new DefaultTableModel(skelbimai,
				new String[] { "Labirinto dydis", "Prisijunge", "Max zaideju" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		lentele.setModel(lentelesmodelis);
		lentele.setFillsViewportHeight(true);
		lentelescroll = new JScrollPane(lentele);

		noticeboard = new JPanel();
		noticeboard.add(lentelescroll);

		pradetizaidima = new JButton();
		pradetizaidima.setText("Pradeti zaidima");
		sukurtizaidima = new JButton();
		sukurtizaidima.setText("Sukurti zaidima");
		sukurtizaidima.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				iskviestiPartijosNustatymus();

			}

		});
		zaidimo_nustatymai = new JButton();
		zaidimo_nustatymai.setText("Nustatymai");
		zaidimo_nustatymai.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				iskviestiProgramosNustatymus();
			}

		});
		prisijungti = new JButton();
		prisijungti.setText("Jungtis prie zaidimo");
		prisijungti.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				prisijungtiPrieZaidimo();
			}

		});

		ivestis = new JTextArea(1, 60);
		ivesti = new JButton();
		ivesti.setText("Ivesti");
		ivesti.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				siustiKomanda(ivestis.getText());
				ivestis.setText("");
			}

		});

		txt = new JTextArea(10, 60);
		scroll = new JScrollPane(txt);
		con = new PrintStream(new TextAreaOutputStream(txt));
		System.setOut(con);
		System.setErr(con);

		buttonpanel.add(sukurtizaidima);
		buttonpanel.add(pradetizaidima);
		buttonpanel.add(prisijungti);
		buttonpanel.add(zaidimo_nustatymai);

		consolepanel.add(scroll, BorderLayout.PAGE_START);
		consolepanel.add(ivestis, BorderLayout.LINE_START);
		consolepanel.add(ivesti, BorderLayout.LINE_END);

		noticeboard.setSize(750, 500);
		frame.add(noticeboard, BorderLayout.LINE_START);
		buttonpanel.setSize(50, 400);
		frame.add(buttonpanel, BorderLayout.LINE_END);
		consolepanel.setSize(800, 50);
		frame.add(consolepanel, BorderLayout.PAGE_END);

		frame.setVisible(true);
	}

	public void iskviestiPartijosNustatymus() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Partijos nustatymai");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setSize(300, 200);
				frame.setLocationRelativeTo(null);
				JPanel panel = new JPanel();
				JPanel radio = new JPanel();
				panel.setLayout(new GridLayout(3, 2, 30, 30));
				JLabel label1 = new JLabel("Labirinto dydis: ");
				JRadioButton rbutt5 = new JRadioButton("5");
				JRadioButton rbutt7 = new JRadioButton("7");
				JRadioButton rbutt10 = new JRadioButton("10");
				JLabel label2 = new JLabel("Zaideju skaicius: ");
				JTextArea text2 = new JTextArea();
				JButton ok = new JButton("Gerai");
				ok.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						if (!text2.getText().equals("")) {
							if (rbutt5.isSelected()) {
								send(new Message(Message.SYSTEM_MSG, 1, "5 " + text2.getText(), null));
								frame.dispose();
							} else if (!rbutt7.isSelected() && !rbutt10.isSelected()) {
								JOptionPane.showMessageDialog(frame, "Uzpildykite visus laukus");
							}
							if (rbutt7.isSelected()) {
								send(new Message(Message.SYSTEM_MSG, 1, "7 " + text2.getText(), null));
								frame.dispose();
							} else if (!rbutt5.isSelected() && !rbutt10.isSelected()) {
								JOptionPane.showMessageDialog(frame, "Uzpildykite visus laukus");
							}
							if (rbutt10.isSelected()) {
								send(new Message(Message.SYSTEM_MSG, 1, "10 " + text2.getText(), null));
								frame.dispose();
							} else if (!rbutt5.isSelected() && !rbutt7.isSelected()) {
								JOptionPane.showMessageDialog(frame, "Uzpildykite visus laukus");
							}
						} else {
							JOptionPane.showMessageDialog(frame, "Uzpildykite visus laukus");
						}
					}

				});
				JButton cancel = new JButton("Atsaukti");
				cancel.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.dispose();
					}

				});
				panel.add(label1);
				radio.add(rbutt5);
				radio.add(rbutt7);
				radio.add(rbutt10);
				panel.add(radio);
				panel.add(label2);
				panel.add(text2);
				panel.add(ok);
				panel.add(cancel);
				frame.add(panel);
				frame.setVisible(true);
			}
		});

	}

	public void iskviestiProgramosNustatymus() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Zaidimo nustatymai");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setSize(300, 200);
				frame.setLocationRelativeTo(null);
				JPanel panel = new JPanel();
				panel.setLayout(new GridLayout(3, 2, 30, 30));
				JLabel label1 = new JLabel("Serverio IP adresas: ");
				JTextArea text1 = new JTextArea(ip);
				JLabel label2 = new JLabel("Port numeris: ");
				JTextArea text2 = new JTextArea(Integer.toString(port));
				JButton ok = new JButton("Gerai");
				ok.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						if (!text1.getText().equals("") && !text2.getText().equals("")) {
							try {
								PrintWriter rf = new PrintWriter("nustatymai.txt");
								rf.println(text1.getText());
								rf.println(text2.getText());
								rf.close();
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
							JOptionPane.showMessageDialog(frame,
									"Kad isigaliotu nauji nustatymai, perkraukite klienta");
							System.out.println("Kad isigaliotu nauji nustatymai, perkraukite klienta");
							frame.dispose();
						} else {
							JOptionPane.showMessageDialog(frame, "Uzpildykite visus laukus");
						}
					}

				});
				JButton cancel = new JButton("Atsaukti");
				cancel.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.dispose();
					}

				});
				panel.add(label1);
				panel.add(text1);
				panel.add(label2);
				panel.add(text2);
				panel.add(ok);
				panel.add(cancel);
				frame.add(panel);
				frame.setVisible(true);
			}
		});
	}
}
