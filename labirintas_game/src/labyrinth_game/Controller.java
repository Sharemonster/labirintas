package labyrinth_game;

import java.util.ArrayList;

import labyrinth_game.Cell.Armory;
import labyrinth_game.Cell.Infirmary;
import labyrinth_game.Cell.Lootable;
import labyrinth_game.Cell.Teleportable;
import labyrinth_game.Control.Fighting;
import labyrinth_game.Control.Movement;
import labyrinth_game.Game.GameMap;
import labyrinth_game.Game.MoveResult;
import labyrinth_game.Game.Player;

public class Controller {
	GameMap gameMap;
	int[] movementVector = new int[] { 0, -1, 1, 0, 0, 1, -1, 0 };

	public Controller(GameMap gameMap) {
		this.gameMap = gameMap;
	}

	public MoveResult executeMove(int command, int direction, Player playing, Player[] players) {
		MoveResult r = new MoveResult();
		ArrayList<String> s = new ArrayList<String>();
		switch (command) {
		case 0:
		case 1:
		case 2:
		case 3: {
			r.setResult(Movement.move(gameMap.getGameMap(), playing, command, s));
			break;
		}
		case 4: {
			return takeTreasure(playing);
		}
		case 5: {
			return dropTreasure(playing);
		}
		case 6: {
			r.setResult(Fighting.useWeapon(gameMap.getGameMap(), playing, players, 0, direction, s));
			break;
		}
		case 7:
		case 8:
		case 9:
		case 10: {
			r.setResult(Fighting.useWeapon(gameMap.getGameMap(), playing, players, 1, direction, s));
			break;
		}
		case 11:
		case 12:
		case 13:
		case 14: {
			r.setResult(Fighting.useWeapon(gameMap.getGameMap(), playing, players, 2, direction, s));
			break;
		}
		}
		for (String a : s) {
			r.appendMessage(a);
		}
		return r;
	}

	public MoveResult takeTreasure(Player playing) {
		MoveResult result = new MoveResult();

		if (gameMap.getGameMap()[playing.getX()][playing.getY()].getClass().getSimpleName().equals("Land")) {
			if (((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()]).getTreasure() > 0) {
				int temp = playing.getTreasure();
				playing.setTreasure(((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()]).getTreasure());
				((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()]).setTreasure(temp);

				result.setResult(1);
				result.appendMessage("Paimei lobi");
			}
		} else {
			result.appendMessage("Nera ka paimti");
		}
		return result;
	}

	public MoveResult dropTreasure(Player playing) {
		MoveResult result = new MoveResult();

		if (gameMap.getGameMap()[playing.getX()][playing.getY()].getClass().getSimpleName().equals("Land")) {
			if (playing.getTreasure() > 0) {
				if (((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()]).getTreasure() == 0) {
					((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()])
							.setTreasure(playing.getTreasure());
					playing.setTreasure(0);

					result.setResult(1);
					result.appendMessage("Ismetei lobi");
				} else {
					int temp = ((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()]).getTreasure();
					((Lootable) gameMap.getGameMap()[playing.getX()][playing.getY()])
							.setTreasure(playing.getTreasure());
					playing.setTreasure(temp);

					result.setResult(1);
					result.appendMessage("Pakeitei lobi");
				}
			} else {
				result.appendMessage("Neturi lobio");
			}
		} else {
			result.appendMessage("Cia negalima ismesti lobio");
		}

		return result;
	}
}
