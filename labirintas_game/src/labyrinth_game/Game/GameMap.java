package labyrinth_game.Game;

import java.util.concurrent.ThreadLocalRandom;

import labyrinth_game.Cell.*;

public class GameMap {

	Cell[][] gameMap;
	static int river_length = 0;
	static int max_river_length = 0;
	static int max_hole_count = 0;

	// Create game map
	public GameMap(int mapSize) {

		gameMap = new Cell[mapSize][mapSize];

		initialiseMapSettings(mapSize);
		initialiseMap();

		LabyrinthCreation.createRiver(gameMap);

		do {
			int i, j;
			do {
				i = ThreadLocalRandom.current().nextInt(0, gameMap.length);
				j = ThreadLocalRandom.current().nextInt(0, gameMap.length);
			} while (isClass(i, j, new String[] { "River", "RiverMouth", "Swamp" }));
			LabyrinthCreation.createLabyrinth(gameMap, i, j, getMaxRiverLength() + 2, getMaxRiverLength());
		} while (getMaxRiverLength() < gameMap.length * gameMap.length);

		LabyrinthCreation.createIndestructableWalls(gameMap);
		LabyrinthCreation.createExit(gameMap);

		LabyrinthCreation.createHoles(gameMap);

		checkWalls();

		createSpecialCells();
	}

	private void createSpecialCells() {
		int cellReachability = LabyrinthCreation.calculateAvailability(gameMap);

		if (cellReachability >= 2 || cellReachability == 1)
			LabyrinthCreation.createSpecialCells(gameMap, 2, false);
		if (cellReachability == 0 || cellReachability == 1)
			LabyrinthCreation.createSpecialCells(gameMap, 1, true);
		if (cellReachability >= 2)
			LabyrinthCreation.createSpecialCells(gameMap, 1, false);
		if (cellReachability == 0)
			LabyrinthCreation.createSpecialCells(gameMap, 2, true);
		LabyrinthCreation.createSpecialCells(gameMap, 4, true);
		LabyrinthCreation.createSpecialCells(gameMap, 3, true);
	}

	private void checkWalls() {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				for (int k = 0; k < 4; k++) {
					if (gameMap[i][j].getWall(k) == -1) {
						gameMap[i][j].setWall(k, 0);
					}
				}
			}
		}
	}

	public boolean isClass(int i, int j, String[] classNames) {
		boolean isClass = false;
		for (String className : classNames) {
			if (gameMap[i][j].getClass().getSimpleName().equals(className))
				isClass = true;
		}
		return isClass;
	}

	private void initialiseMap() {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				gameMap[i][j] = new Land(i, j);
				gameMap[i][j].setX(i);
				gameMap[i][j].setY(j);
			}
		}
	}

	private void initialiseMapSettings(int mapSize) {
		switch (mapSize) {
		case 1:
		case 2:
		case 3:
		case 4: {
			river_length = 4;
			max_hole_count = 2;
			break;
		}
		case 5: {
			river_length = ThreadLocalRandom.current().nextInt(7, 9);
			max_hole_count = ThreadLocalRandom.current().nextInt(5, 7);
			break;
		}
		default: {
			river_length = ThreadLocalRandom.current().nextInt(17, 19);
			max_hole_count = ThreadLocalRandom.current().nextInt(13, 16);
		}
		}
	}

	public static int getRiverLength() {
		return river_length;
	}

	public static int getMaxRiverLength() {
		return max_river_length;
	}

	public static void setMaxRiverLength(int nr) {
		max_river_length = nr;
	}

	public static void incrementMaxRiverLength() {
		max_river_length++;
	}

	public static int getMaxHoleCount() {
		return max_hole_count;
	}

	public int getMapSize() {
		return gameMap.length;
	}

	public String printGameMap() {
		String resultString = "";

		String line = "_____________________________________________________________\n";

		resultString += line;
		resultString = writeHoleList(resultString) + line;
		resultString = writeWalls(resultString) + line + "\n";
		resultString = writeWallList(resultString) + line;
		resultString = writeCellList(resultString, line) + line;

		return resultString;
	}

	private String writeCellList(String resultString, String line) {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				resultString += gameMap[j][i].toString();
				resultString += "\n";
				resultString += line;
			}
		}
		return resultString;
	}

	private String writeWallList(String resultString) {
		for (int i = 0; i < gameMap.length; i++) {
			resultString += "||  ";
			for (int j = 0; j < gameMap.length; j++) {
				for (int k = 0; k < 4; k++)
					resultString += gameMap[j][i].getWall(k);
				resultString += "  ||  ";
			}
			resultString += "\n";
		}
		return resultString;
	}

	private String writeHoleList(String resultString) {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				if (isClass(i, j, new String[] { "Hole" })) {
					try {
						resultString += gameMap[i][j].getX() + " " + gameMap[i][j].getY() + " --> "
								+ ((Hole) gameMap[i][j]).getNextCell().getX() + " "
								+ ((Hole) gameMap[i][j]).getNextCell().getY() + "\n";
					} catch (NullPointerException e) {
						System.out.println(e.getMessage());
						System.out.println("X: " + i + " Y: " + j);
						resultString += (gameMap[i][j].getX() + " " + gameMap[i][j].getY() + " -->  ???\n");
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}
		}
		return resultString;
	}

	private String writeWalls(String resultString) {
		String verticalWall = "+";
		String horizontalWall = "++++++++++++";
		String verticalPath = " ";
		String horizontalPath = "            ";
		String verticalExit = "|";
		String horizontalExit = "------------";

		resultString = writeTopWall(resultString, horizontalWall, horizontalPath, horizontalExit) + "\n";

		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {

				if (gameMap[j][i].getWall(3) == 1 || gameMap[j][i].getWall(3) == 2) {
					resultString += String.format("%s", verticalWall);
				} else if (gameMap[j][i].getWall(3) == 3) {
					resultString += String.format("%s", verticalExit);
				} else {
					resultString += String.format("%s", verticalPath);
				}

				resultString += String.format("%11s", gameMap[j][i].getName());

			}
			if (gameMap[gameMap.length - 1][i].getWall(1) == 1 || gameMap[gameMap.length - 1][i].getWall(1) == 2) {
				resultString += String.format("%s", verticalWall);
			} else if (gameMap[gameMap.length - 1][i].getWall(1) == 3) {
				resultString += String.format("%s", verticalExit);
			} else {
				resultString += String.format("%s", verticalPath);
			}
			resultString += "\n";

			for (int j = 0; j < gameMap.length; j++) {
				if (gameMap[j][i].getWall(2) == 1 || gameMap[j][i].getWall(2) == 2) {
					resultString += String.format("%s", horizontalWall);
				} else if (gameMap[j][i].getWall(2) == 3) {
					resultString += String.format("%s", horizontalExit);
				} else {
					resultString += String.format("%s", horizontalPath);
				}
			}

			resultString += "\n";
		}
		return resultString;
	}

	private String writeTopWall(String resultString, String horizontalWall, String horizontalPath,
			String horizontalExit) {
		for (int j = 0; j < gameMap.length; j++) {
			if (gameMap[j][0].getWall(0) == 1 || gameMap[j][0].getWall(0) == 2) {
				resultString += String.format("%s", horizontalWall);
			} else if (gameMap[j][0].getWall(0) == 3) {
				resultString += String.format("%s", horizontalExit);
			} else {
				resultString += String.format("%s", horizontalPath);
			}
		}
		return resultString;
	}

	public Cell[][] getGameMap() {
		return gameMap;
	}

}
