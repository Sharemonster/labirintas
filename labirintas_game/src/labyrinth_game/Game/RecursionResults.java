package labyrinth_game.Game;

public class RecursionResults {
	boolean recursionEndCondition, loopCondition;
	
	public RecursionResults(boolean recursionEndCondition, boolean loopCondition) {
		this.recursionEndCondition = recursionEndCondition;
		this.loopCondition = loopCondition;
	}
	
	public boolean getRecursionEndCondition() {
		return this.recursionEndCondition;
	}
	public boolean getLoopCondition() {
		return this.loopCondition;
	}
}
