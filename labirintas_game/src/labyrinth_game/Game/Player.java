package labyrinth_game.Game;

import java.util.Arrays;

import labyrinth_game.Logging.Log;

public abstract class Player implements Cloneable {
	int id, lives, treasure, x, y, exitDirection, movementPenalty, illegalMove;
	int[] weapons;
	boolean exited = false, dead = false;
	String name;

	public Player() {
		id = 0;
		weapons = new int[] { 3, 3, 3 };
		lives = 2;
		treasure = 0;
		illegalMove = 0;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public void setMovementPenalty(int penalty) {
		this.movementPenalty = penalty;
	}

	public int getMovementPenalty() {
		return this.movementPenalty;
	}

	public int getTreasure() {

		return this.treasure;

	}

	public void setTreasure(int treasure) {

		this.treasure = treasure;

	}

	public int getLives() {

		return this.lives;

	}

	public void setLives(int lives) {
		this.lives = lives;

	}

	public void setWeapons(int[] weapons) {
		for (int i = 0; i < 3; i++) {
			this.weapons[i] = weapons[i];
		}
		fixWeapons();
	}

	private void fixWeapons() {
		for (int i = 0; i < 3; i++) {
			if (weapons[i] < 0) {
				weapons[i] = 0;
			}
		}
	}

	public int[] getWeapons() {

		return weapons;

	}

	public void transferWeapon(int[] weapon) {

		for (int i = 0; i < weapon.length; i++) {
			this.weapons[i] = weapon[i];
		}
		fixWeapons();

	}

	public void addWeapon(int weaponChange, int weaponID) {
		weapons[weaponID] += weaponChange;
		fixWeapons();
	}

	public void setExited(boolean exited) {
		this.exited = exited;
	}

	public boolean getExited() {
		return exited;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getExitDirection() {
		return exitDirection;
	}

	public void setExitDirection(int direction) {
		this.exitDirection = direction;
	}

	public void setDead(boolean dead) {
		if (dead) {
			this.dead = true;
			this.movementPenalty = 5;
		}
	}

	public boolean checkDead() {
		this.movementPenalty -= 1;
		if (movementPenalty >= 1)
			this.dead = true;
		else
			this.dead = false;

		return this.dead;
	}

	public void injure() {
		lives -= 1;
	}

	public boolean getDead() {
		return dead;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract String getMove(int playerIndex);

	public void toggleIllegalMove() {
		if (this.illegalMove > -1)
			this.illegalMove = -100;
		else
			this.illegalMove = 0;
	}

	public int getIllegalMove() {
		return illegalMove;
	}

	public String toString() {
		String result = "";
		try {
			result += "Player name: " + this.name;
			result += "\nPlayer id: " + this.id;
			result += "\nPlayer x: " + this.x;
			result += "\nPlayer y: " + this.y;
			result += "\nPlayer lives: " + this.lives;
			result += "\nPlayer treasure: " + this.treasure;
			result += "\nPlayer exited: " + this.exited;
			result += "\nPlayer exitDirection: " + this.exitDirection;
			result += "\nPlayer movementPenalty: " + this.movementPenalty;
			result += "\nPlayer illegalMove: " + this.illegalMove;
			result += "\nPlayer dead: " + this.dead;
			for (int i = 0; i < 3; i++) {
				result += "\nPlayer weapon[" + i + "]: " + this.weapons[i];
			}
		} catch (Exception e) {
			Log.printFile("PLAYER", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
		}
		return result;
	}

	public Object clone() {
		try {
			Player clone = (Player) super.clone();
			cloneWeapons(clone);
			return clone;
		} catch (Exception e) {
			Log.printFile("PLAYER", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
			return null;
		}
	}

	private Player cloneWeapons(Player clone) {
		clone.weapons = Arrays.copyOf(this.weapons, weapons.length);

		return clone;
	}
}
