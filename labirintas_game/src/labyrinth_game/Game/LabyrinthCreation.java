package labyrinth_game.Game;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import labyrinth_game.Cell.*;

public class LabyrinthCreation {
	final static int[] directionVector = new int[] { -1, 0, 0, 1, 1, 0, 0, -1 };

	public static void createRiver(Cell[][] labyrinth) {

		Random rnd = new Random();
		int x = 0, y = 0;
		boolean loopCondition = true;

		while (loopCondition) {
			// Pick a random cell for river mouth
			x = rnd.nextInt(labyrinth.length);
			y = rnd.nextInt(labyrinth[0].length);
			// If not out of bounds
			if (x != 0 && x != labyrinth.length - 1 || y != 0 && y != labyrinth[0].length - 1)
				loopCondition = false;
		}
		labyrinth[x][y] = new RiverMouth(x, y);
		labyrinth[x][y].setX(x);
		labyrinth[x][y].setY(y);

		// Create the rest of the river
		continueRiver(labyrinth, x, y, rnd, false);

	}

	// Create the rest of the river
	public static RecursionResults continueRiver(Cell[][] labyrinth, int x, int y, Random rnd, boolean loopCondition) {

		int randomDirectionCondition = rnd.nextInt(2), riverTurn = rnd.nextInt(4), loopCount = 0, u = x, v = y;
		boolean recursionEndCondition = false;
		RecursionResults recursionResults;

		do {
			loopCount += 1;

			// If randomDirectionCondition == 0 turn the river clockwise, else CCW
			if (randomDirectionCondition == 0) {
				if (riverTurn < 3)
					riverTurn += 1;
				else
					riverTurn = 0;
			} else {
				if (riverTurn > 0)
					riverTurn -= 1;
				else
					riverTurn = 3;
			}
			loopCondition = false;

			u = x + directionVector[riverTurn * 2];
			v = y + directionVector[riverTurn * 2 + 1];

			if (inArray(labyrinth, u, v)) {
				if (labyrinth[u][v].getClass().getSimpleName().equals("Land")) {
					if (GameMap.getMaxRiverLength() + 1 <= GameMap.getRiverLength()) {
						if (!recursionEndCondition) {
							createRiverCell(labyrinth, x, y, riverTurn, u, v);
							GameMap.incrementMaxRiverLength();

							recursionResults = continueRiver(labyrinth, u, v, rnd, loopCondition);
							recursionEndCondition = recursionResults.getRecursionEndCondition();
							loopCondition = recursionResults.getLoopCondition();

							// If wrongfully created reset to Land cell
							if (!loopCondition) {
								labyrinth[u][v] = new Land(u, v);
							}
						}
					} else { // If maximum river length is reached create a swamp
						if (labyrinth[u][v].getClass().getSimpleName().equals("Land")) {
							if (!recursionEndCondition) {
								createSwampCell(labyrinth, x, y, riverTurn, u, v);
								loopCondition = true;
								recursionEndCondition = true;
							}
						}
					}
				}
			}

		} while (!loopCondition && loopCount <= 4);

		return new RecursionResults(recursionEndCondition, loopCondition);

	}

	public static void createSwampCell(Cell[][] labyrinth, int x, int y, int riverTurn, int u, int v) {

		labyrinth[u][v] = new Swamp(u, v);

		((Teleportable) labyrinth[x][y]).setPreviousCell(labyrinth[u][v]);

		labyrinth[u][v].setX(u);
		labyrinth[u][v].setY(v);

		labyrinth[x][y].setWall(riverTurn, 0);
		labyrinth[u][v].setWall(riverTurn > 1 ? riverTurn - 2 : riverTurn + 2, 0);
	}

	public static void createRiverCell(Cell[][] labyrinth, int x, int y, int riverTurn, int u, int v) {

		labyrinth[u][v] = new River(u, v);

		labyrinth[u][v].setX(u);
		labyrinth[u][v].setY(v);

		// Destroy walls in the rivers direction of flow
		labyrinth[x][y].setWall(riverTurn, 0);
		labyrinth[u][v].setWall(riverTurn > 1 ? riverTurn - 2 : riverTurn + 2, 0);

		// Set direction of flow
		((Teleportable) labyrinth[u][v]).setNextCell(labyrinth[x][y]);
		((Teleportable) labyrinth[x][y]).setPreviousCell(labyrinth[u][v]);
		((Teleportable) labyrinth[u][v]).setRiverNumber(((Teleportable) labyrinth[x][y]).getRiverNumber() + 1);
	}

	// Create holes in the labyrinth
	public static void createHoles(Cell[][] labyrinth) {

		int[] holeGroup = new int[8];
		// x,y u,v k,l - cell coordinates j, i - loop indices
		int index, selectedHoleGroup, x = -1, y = -1, j = -1, i = -1, u = -1, v = -1, k = -1, l = -1;
		boolean continueCondition = true;

		// Defines amount of interconnected holes
		holeGroup[0] = ThreadLocalRandom.current().nextInt(1, 4);
		holeGroup[1] = ThreadLocalRandom.current().nextInt(2, 4);
		if (labyrinth.length != 5)
			holeGroup[2] = ThreadLocalRandom.current().nextInt(3, 5);
		else
			holeGroup[2] = 0;
		if (labyrinth.length == 7)
			holeGroup[3] = ThreadLocalRandom.current().nextInt(4, 6);
		else
			holeGroup[3] = 0;

		// Checks if enough holes defined
		int holeDifference = GameMap.getMaxHoleCount() - holeGroup[0] - holeGroup[1] - holeGroup[2] - holeGroup[3];

		if (holeDifference > 0) {
			if (holeGroup[2] == 0)
				holeGroup[2] = holeDifference;
			else {
				if (holeGroup[3] == 0)
					holeGroup[3] = holeDifference;
				else
					holeGroup[4] = holeDifference;
			}
		}
		if (holeDifference < 0) {
			holeGroup[1] += holeDifference;
		}

		do {
			selectedHoleGroup = 0;
			do {
				continueCondition = true;
				index = 0;
				int[] coordinates = new int[2];

				// Search for free cell to place hole
				do {
					coordinates = findFreeCell(labyrinth);
					x = coordinates[0];
					y = coordinates[1];
					index++;
				} while (index <= 4 * labyrinth.length * labyrinth.length && !isNearRiverHole(labyrinth, x, y));

				if (index > 4 * labyrinth.length * labyrinth.length)
					continueCondition = false;
				else {
					createHoleCell(labyrinth, x, y);
					// Current holes coordinates saved for connecting last hole
					k = x;
					l = y;
				}
				j = holeGroup[selectedHoleGroup];

				// Create rest of holes in the group
				while (j > 1 && continueCondition) {
					u = x;
					v = y;

					// i defines distance between holes
					if (holeGroup[selectedHoleGroup] > 2)
						i = 2;
					else
						i = 5;

					index = 0;

					// Find a free cell in a certain distance from previous hole
					do {
						coordinates = findFreeCell(labyrinth);
						x = coordinates[0];
						y = coordinates[1];
						index++;

					} while ((Math.sqrt(x - u) + Math.sqrt(y - v) < i || !isNearRiverHole(labyrinth, x, y))
							&& index <= 4 * labyrinth.length * labyrinth.length);

					if (index > 4 * labyrinth.length * labyrinth.length)
						continueCondition = false;
					else {
						// Create hole and connect it to the previous hole in the group
						createHoleCell(labyrinth, x, y);
						((Teleportable) labyrinth[u][v]).setNextCell(labyrinth[x][y]);
						((Teleportable) labyrinth[x][y]).setPreviousCell(labyrinth[u][v]);
					}
					j--;
				}

				// When done connect last hole with the first
				if (continueCondition) {
					((Teleportable) labyrinth[x][y]).setNextCell(labyrinth[k][l]);
					((Teleportable) labyrinth[k][l]).setPreviousCell(labyrinth[x][y]);
				}
				selectedHoleGroup++;

			} while (selectedHoleGroup <= 5 && holeGroup[selectedHoleGroup] != 0 && continueCondition);

			if (!continueCondition) {
				for (i = 0; i < labyrinth.length; i++) {
					for (j = 0; j < labyrinth[i].length; j++) {
						// if (!labyrinth[i][j].getClass().getSimpleName().equals("Land")) {
						if (labyrinth[i][j].getClass().getSimpleName().equals("Hole")) {
							Cell temporary = labyrinth[i][j];
							labyrinth[i][j] = new Land(x, y);
							for (int direction = 0; direction < 4; direction++) {
								labyrinth[i][j].setWall(direction, temporary.getWall(direction));
							}
						}
						// }
					}
				}
			}
		} while (!continueCondition);

	}

	public static void createHoleCell(Cell[][] labyrinth, int x, int y) {
		Cell temporary = labyrinth[x][y];
		labyrinth[x][y] = new Hole(x, y);
		labyrinth[x][y].setX(x);
		labyrinth[x][y].setY(y);
		for (int direction = 0; direction < 4; direction++) {
			labyrinth[x][y].setWall(direction, temporary.getWall(direction));
		}
	}

	// Checks if a cell is near a river or a hole
	public static boolean isNearRiverHole(Cell[][] labyrinth, int x, int y) {
		int i = 0, j = 0;
		boolean isNear = true;
		for (int w = 0; w < 4; w++) {
			if ((y + directionVector[2 * w + 1] >= 0 && x + directionVector[2 * w] >= 0
					&& y + directionVector[2 * w + 1] < labyrinth.length
					&& x + directionVector[2 * w] < labyrinth.length)
					&& !labyrinth[x + directionVector[2 * w]][y + directionVector[2 * w + 1]].getClass().getSimpleName()
							.equals("Land")) {
				if (labyrinth[x + directionVector[2 * w]][y + directionVector[2 * w + 1]].getClass().getSimpleName()
						.equals("River")) {
					i++;
				} else if (labyrinth[x + directionVector[2 * w]][y + directionVector[2 * w + 1]].getClass()
						.getSimpleName().equals("Hole")) {
					j = 1;
				}

				if (i > 1 || j == 1)
					isNear = false;
				else
					isNear = true;

			}
		}
		return isNear;
	}

	// Finds a random free cell
	public static int[] findFreeCell(Cell[][] labyrinth) {
		int x, y;
		do {
			x = ThreadLocalRandom.current().nextInt(0, labyrinth.length);
			y = ThreadLocalRandom.current().nextInt(0, labyrinth.length);
		} while (!labyrinth[x][y].getClass().getSimpleName().equals("Land"));

		return new int[] { x, y };
	}

	// Creates indestructable walls on arrays outer edges
	public static void createIndestructableWalls(Cell[][] labyrinth) {
		for (int i = 0; i < labyrinth.length; i++) {
			labyrinth[0][i].setWall(3, 2);
			labyrinth[labyrinth[i].length - 1][i].setWall(1, 2);
			for (int j = 0; j < labyrinth[i].length; j++) {
				if (i == 0)
					labyrinth[j][i].setWall(0, 2);
				if (i == labyrinth.length - 1)
					labyrinth[j][i].setWall(2, 2);
			}
		}
	}

	// Destroys walls to create a labyrinth
	public static boolean createLabyrinth(Cell[][] labyrinth, int x, int y, int i, int min) {
		int loopCount = 0, directionCondition = ThreadLocalRandom.current().nextInt(0, 2),
				direction = ThreadLocalRandom.current().nextInt(0, 4);
		int u, v, max = labyrinth.length * labyrinth.length;
		boolean continueCondition;
		// Determine path direction
		do {

			loopCount++;
			if (directionCondition == 0) {
				if (direction != 3)
					direction += 1;
				else
					direction = 0;
			} else {
				if (direction != 0)
					direction -= 1;
				else
					direction = 3;
			}
			continueCondition = false;

			u = x + directionVector[2 * direction];
			v = y + directionVector[2 * direction + 1];

			// If not a piece of river make path
			if (inArray(labyrinth, u, v)) {
				if (!labyrinth[u][v].getClass().getSimpleName().equals("River")
						&& !labyrinth[u][v].getClass().getSimpleName().equals("RiverMouth")
						&& !labyrinth[u][v].getClass().getSimpleName().equals("Swamp")) {
					if (labyrinth[x][y].getWall(direction) == 1
							&& labyrinth[u][v].getWall(direction > 1 ? direction - 2 : direction + 2) == 1) {

						labyrinth[x][y].setWall(direction, 0);
						labyrinth[u][v].setWall(direction > 1 ? direction - 2 : direction + 2, 0);

					}
					// Continue carving a path
					if (i <= max) {
						continueCondition = createLabyrinth(labyrinth, u, v, GameMap.getMaxRiverLength() + 2,
								GameMap.getMaxRiverLength());
						if (!continueCondition) {
							// FIXME Fix labyrinth generation
							/*
							 * if (labyrinth[x][y].getWall(direction) == 0 &&
							 * labyrinth[u][v].getWall(direction > 1 ? direction - 2 : direction + 2) == 0)
							 * { labyrinth[x][y].setWall(direction, 1); labyrinth[u][v].setWall(direction >
							 * 1 ? direction - 2 : direction + 2, 1); Log.printConsole("CREATION",
							 * "REVERTING WALLS");
							 */
							GameMap.setMaxRiverLength(i);
							continueCondition = true;
							// }
						}
					} else {
						continueCondition = true;
						GameMap.setMaxRiverLength(i);
					}
				} else if (((Teleportable) labyrinth[u][v]).getRiverNumber() <= min) {
					if (labyrinth[x][y].getWall(direction) == 1
							&& labyrinth[u][v].getWall(direction > 1 ? direction - 2 : direction + 2) == 1) {
						labyrinth[x][y].setWall(direction, 0);
						labyrinth[u][v].setWall(direction > 1 ? direction - 2 : direction + 2, 0);
						continueCondition = true;
						GameMap.setMaxRiverLength(i - 1);
					}
				}
			}
		} while (!continueCondition && loopCount > 4);

		return continueCondition;
	}

	public static boolean inArray(Cell[][] labyrinth, int x, int y) {
		return x < labyrinth.length && x >= 0 && y < labyrinth[0].length && y >= 0;
	}

	// Calculates how hard it is to reach a cell
	public static int calculateAvailability(Cell[][] labyrinth) {
		int availability, specialCellCount = 0;

		for (int i = 0; i < labyrinth.length; i++) {
			for (int j = 0; j < labyrinth[i].length; j++) {

				if (labyrinth[i][j].getClass().getSimpleName().equals("Land")) {
					labyrinth[i][j].setSpecialAvailability(true);
					labyrinth[i][j].setAccessDifficulty(0);

					for (int k = 0; k < 4; k++) {
						availability = labyrinth[i][j].getAccessDifficulty();

						if (i + directionVector[2 * k] >= 0 && i + directionVector[2 * k] < labyrinth.length
								&& j + directionVector[2 * k + 1] >= 0
								&& j + directionVector[2 * k + 1] < labyrinth.length) {

							if (labyrinth[i + directionVector[2 * k]][j + directionVector[2 * k + 1]].getClass()
									.getSimpleName().equals("River")) {
								labyrinth[i][j].setAccessDifficulty(labyrinth[i][j].getAccessDifficulty() + 6);

							} else if (labyrinth[i + directionVector[2 * k]][j + directionVector[2 * k + 1]].getClass()
									.getSimpleName().equals("Hole")) {

								labyrinth[i][j].setAccessDifficulty(labyrinth[i][j].getAccessDifficulty() + 5);
							}
							if (labyrinth[i][j].getWall(k) == 1 || labyrinth[i][j].getWall(k) == 2) {

								if (i + directionVector[2 * k] == 0
										|| i + directionVector[2 * k] == labyrinth.length - 1
										|| j + directionVector[2 * k + 1] == 0
										|| j + directionVector[2 * k + 1] == labyrinth.length - 1) {
									labyrinth[i][j].setAccessDifficulty(labyrinth[i][j].getAccessDifficulty() + 6);

								} else {
									labyrinth[i][j].setAccessDifficulty(labyrinth[i][j].getAccessDifficulty() + 4);

								}
							}
							if (availability == labyrinth[i][j].getAccessDifficulty()) {
								labyrinth[i][j].setSpecialAvailability(false);
							}
						}
					}
					if (labyrinth[i][j].getSpecialAvailability()) {
						specialCellCount++;
					}
				}
			}
		}
		return specialCellCount;
	}

	// Create Infirmary and Armory cells or place treasure
	public static void createSpecialCells(Cell[][] labyrinth, int cellID, boolean continueCondition) {
		int i = 0, j = 0, maxAccessDifficulty = 0, x = -1, y = -1;
		maxAccessDifficulty = 0;

		// Find cell with highest access difficulty
		for (i = 0; i < labyrinth.length; i++) {
			for (j = 0; j < labyrinth[i].length; j++) {
				if (labyrinth[i][j].getAccessDifficulty() > maxAccessDifficulty) {
					maxAccessDifficulty = labyrinth[i][j].getAccessDifficulty();
					x = i;
					y = j;
				}
			}
		}
		// Zero out access difficulty to remove from further processing
		if (x != -1 && y != -1)
			labyrinth[x][y].setAccessDifficulty(0);

		if (i >= labyrinth.length)
			i = labyrinth.length - 1;
		if (j >= labyrinth.length)
			j = labyrinth.length - 1;

		// Create a cell or treasure
		switch (cellID) {
		case 1:
			((Lootable) labyrinth[x][y]).setTreasure(cellID); // Creates fake treasure
			break;
		case 2:
			((Lootable) labyrinth[x][y]).setTreasure(cellID); // Creates real treasure
			break;
		case 3: {
			Cell laik = labyrinth[x][y];
			labyrinth[x][y] = new Armory(x, y);
			labyrinth[x][y].setX(x);
			labyrinth[x][y].setY(y);
			for (int k = 0; k < 4; k++) {
				labyrinth[x][y].setWall(k, laik.getWall(k));
			}
			break;
		}
		case 4: {
			Cell laik = labyrinth[x][y];
			labyrinth[x][y] = new Infirmary(x, y);
			labyrinth[x][y].setX(x);
			labyrinth[x][y].setY(y);
			for (int k = 0; k < 4; k++) {
				labyrinth[x][y].setWall(k, laik.getWall(k));
			}
			break;
		}
		}
	}

	// Creates an exit
	public static void createExit(Cell[][] labyrinth) {
		int x = -1, y = 0, i = ThreadLocalRandom.current().nextInt(0, labyrinth.length),
				direction = ThreadLocalRandom.current().nextInt(0, 4);

		do {
			switch (direction) {
			case 0: {
				y = 0;
				break;
			}
			case 1: {
				x = labyrinth.length - 1;
				break;
			}
			case 2: {
				y = labyrinth.length - 1;
				break;
			}
			case 3: {
				x = 0;
				break;
			}
			}
			if (x == -1)
				x = i;
			else
				y = i;

		} while (labyrinth[x][y].getClass().getSimpleName().equals("River")
				&& (((Teleportable) labyrinth[x][y]).getRiverNumber() <= GameMap.max_river_length + 1
						&& ((Teleportable) labyrinth[x][y]).getRiverNumber() >= GameMap.max_river_length - 1));

		labyrinth[x][y].setWall(direction, 3);
	}

}
