package labyrinth_game.Game;

import java.util.Scanner;

public class HumanPlayer extends Player {

	int id, lives, treasure, x, y, exitDirection, movementPenalty;
	int[] weapon;
	boolean exited = false, dead = false;
	String name;
	Scanner sc = new Scanner(System.in);

	public HumanPlayer() {
		super();
	}

	public String getMove(int playerIndex) {
		String move = "";
		String in = sc.nextLine();

		if (in.length() == 1) {
			move = in + " 0";
		} else if (in.length() == 2) {
			try {
				int o = Integer.parseInt(in);
				move = in + " 0";
			} catch (Exception e) {
				move = in + "0";
			}
		} else {
			move = in;
		}

		return move;
	}

}
