package labyrinth_game.Game;

public class MoveResult {
	String message;
	int result;
	HumanPlayer deadplayer;

	public MoveResult() {
		message = "";
		result = 0;
		deadplayer = null;
	}

	public void appendMessage(String message) {
		this.message += message;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public int getResult() {
		return result;
	}

	public void setDeadPlayer(HumanPlayer player) {
		this.deadplayer = player;
	}

	public HumanPlayer getDeadPlayer() {
		return deadplayer;
	}
}
