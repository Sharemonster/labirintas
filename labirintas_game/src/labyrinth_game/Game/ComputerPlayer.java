package labyrinth_game.Game;

import labyrinth_game.AI.DecisionMaker;
import labyrinth_game.AI.GameState;

public class ComputerPlayer extends Player {

	int id, lives, treasure, x, y, exitDirection, movementPenalty;
	int[] weapon;
	boolean exited = false, dead = false;
	String name;
	DecisionMaker dm = new DecisionMaker();
	GameState state;

	public ComputerPlayer() {
		super();
	}

	public void setGameState(GameState state) {
		this.state = state;
	}

	public String getMove(int playerIndex) {
		int move = dm.findMove(state, playerIndex);
		/*
		 * Legitimate moves: 0 - Move Up 1 - Move Right 2 - Move Down 3 - Move Left 4 -
		 * Pick Up Treasure 5 - Drop Treasure 6 - Use Knife 7 - Shoot Gun Up 8 - Shoot
		 * Gun Right 9 - Shoot Gun Down 10 - Shoot Gun Left 11 - Throw Grenade Up 12 -
		 * Throw Grenade Right 13 - Throw Grenade Down 14 - Throw Grenade Left
		 */
		switch (move) {
		case 0: {
			return "0 0";
		}
		case 1: {
			return "1 0";
		}
		case 2: {
			return "2 0";
		}
		case 3: {
			return "3 0";
		}
		case 4: {
			return "4 0";
		}
		case 5: {
			return "5 0";
		}
		case 6: {
			return "6 0";
		}
		case 7: {
			return "7 0";
		}
		case 8: {
			return "8 1";
		}
		case 9: {
			return "9 2";
		}
		case 10: {
			return "10 3";
		}
		case 11: {
			return "11 0";
		}
		case 12: {
			return "12 1";
		}
		case 13: {
			return "13 2";
		}
		case 14: {
			return "14 3";
		}
		}
		return Integer.toString(move);
	}

}
