package labyrinth_game.Logging;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.util.Arrays;

public class Log {
	static Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	public static void printConsole(String tag, String message) {
		timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp.toString() + " | " + tag + " | " + message);
	}

//TODO Create a log deletion method
	// TODO Remove some logging
	public static void printFile(String tag, String message, int fileName) {
		Path file;
		switch (fileName) {
		case 0: {
			file = Paths.get("labyrinth-log.txt");
			break;
		}
		case 1: {
			file = Paths.get("ai-log.txt");
			break;
		}
		case 2: {
			file = Paths.get("game-log.txt");
			break;
		}
		case 3: {
			file = Paths.get("gamestate.txt");
			break;
		}
		case 4: {
			file = Paths.get("exceptions.txt");
			break;
		}
		case 5: {
			file = Paths.get("links.txt");
			break;
		}
		case 6:{
			file = Paths.get("test.txt");
			break;
		}
		default: {
			file = Paths.get("wtf.txt");
			break;
		}
		}
		timestamp = new Timestamp(System.currentTimeMillis());
		try {
			Files.write(file, Arrays.asList(timestamp.toString() + " | " + tag + " | " + message),
					Charset.forName("UTF-8"), StandardOpenOption.APPEND);
		} catch (Exception e) {
			//Log.printFile("LOG", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
		}
	}
}
