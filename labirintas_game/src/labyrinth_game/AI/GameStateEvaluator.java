package labyrinth_game.AI;

import labyrinth_game.Game.Player;

public interface GameStateEvaluator {
	public double calculateGrenadeValue(int playerIndex, Player[] players);

	public double calculateKnifeValue(int playerIndex, Player[] players);

	public double calculateGunValue(int playerIndex, Player[] players);

	public double calculateTreasureValue(int playerIndex, Player[] players);

	public double calculateSelfLivesValue(int playerIndex, Player[] players);

	public double calculateComparedLivesValue(int playerIndex, Player[] players);

	public double calculateIllegalMove(int playerIndex, Player[] players);

	public double calculateWalls(int oldWalls, int newWalls);

	public double calculateMovement(int playerIndex, Player[] players, int oldX, int oldY);
}
