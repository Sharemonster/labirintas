package labyrinth_game.AI;

import java.util.Random;

import labyrinth_game.Logging.Log;

public class DecisionMaker {
	int turn = -1;
	int playerIndex = 0;
	Random rnd = new Random();

	public int findMove(GameState state, int playerIndex) {
		this.turn = -1;
		this.playerIndex = playerIndex;
		int maxDepth = 10;

		long start, finish;

		//start = System.currentTimeMillis();
		//minimax(0, maxDepth, playerIndex, state);
		//finish = System.currentTimeMillis();
		//Log.printFile("AI", finish - start + " for regular minimax with " + maxDepth + " depth", 6);
		//Log.printConsole("AI", finish - start + " for regular minimax with " + maxDepth + " depth");
		//start = System.currentTimeMillis();
		minimaxAB(0, maxDepth, playerIndex, state, -10000, 10000);
		//finish = System.currentTimeMillis();
		//Log.printFile("AI", finish - start + " for alpha beta minimax with " + maxDepth + " depth", 6);
		//Log.printConsole("AI", finish - start + " for alpha beta minimax with " + maxDepth + " depth");

		//Log.printConsole("AI", "Minimax returned: " + turn);
		//Log.printFile("AI", "Minimax returned: " + turn, 1);

		return turn;
	}

	private double minimax(int depth, int maxDepth, int playerIndex, GameState state) {
		double maxScore = -10;
		double oldScore = -10;

		if (depth >= maxDepth) {
			return state.getStateValue();
		}

		if (playerIndex == this.playerIndex) {
			/*
			 * Legitimate moves: 0 - Move Up 1 - Move Right 2 - Move Down 3 - Move Left 4 -
			 * Pick Up Treasure 5 - Drop Treasure 6 - Use Knife 7 - Shoot Gun Up 8 - Shoot
			 * Gun Right 9 - Shoot Gun Down 10 - Shoot Gun Left 11 - Throw Grenade Up 12 -
			 * Throw Grenade Right 13 - Throw Grenade Down 14 - Throw Grenade Left
			 */
			if (state.getStateValue() > -200) {
				for (int moveIndex = 0; moveIndex < 15; moveIndex++) {

					double score = minimax(depth + 1, maxDepth,
							playerIndex + 1 > state.getPlayerCount() - 1 ? 0 : playerIndex + 1,
							new GameState(state, this.playerIndex, moveIndex));

					if (state.players[playerIndex].getIllegalMove() < 0)
						state.players[playerIndex].toggleIllegalMove();

					if (score > -200) {
						if (rnd.nextInt(100) == 10)
							score = 100;
					}

					maxScore = Math.max(maxScore, score);

					if (depth == 0) {
						if (maxScore > oldScore) {
							this.turn = moveIndex;
							Log.printConsole("MINIMAX", "playerIndex: " + playerIndex + " | oldScore: " + oldScore
									+ " | maxScore: " + maxScore + " | Modified turn: " + this.turn);
							oldScore = maxScore;
						}
					}
				}
			}
		} else {
			if (state.getStateValue() > -200) {
				for (int moveIndex = 0; moveIndex < 15; moveIndex++) {

					double score = minimax(depth + 1, maxDepth,
							playerIndex + 1 > state.getPlayerCount() - 1 ? 0 : playerIndex + 1,
							new GameState(state, this.playerIndex, moveIndex));

					if (state.players[playerIndex].getIllegalMove() < 0)
						state.players[playerIndex].toggleIllegalMove();

					if (score > -200) {
						if (rnd.nextInt(100) == 10)
							score = -100;
					}

					if (maxScore == -10 && score > -200)
						maxScore = score;
					else if (score > -200)
						maxScore = Math.min(maxScore, score);

					if (depth == 0) {
						if (maxScore < oldScore) {
							this.turn = moveIndex;
							Log.printConsole("MINIMAX", "playerIndex: " + playerIndex + " | oldScore: " + oldScore
									+ " | maxScore: " + maxScore + " | Modified turn: " + this.turn);
							oldScore = maxScore;
						}
					}
				}
			}
		}
		return maxScore;
	}

	private double minimaxAB(int depth, int maxDepth, int playerIndex, GameState state, double alpha, double beta) {
		double maxScore = -10;
		double oldScore = -10;

		if (depth >= maxDepth) {
			return state.getStateValue();
		}

		if (playerIndex == this.playerIndex) {
			/*
			 * Legitimate moves: 0 - Move Up 1 - Move Right 2 - Move Down 3 - Move Left 4 -
			 * Pick Up Treasure 5 - Drop Treasure 6 - Use Knife 7 - Shoot Gun Up 8 - Shoot
			 * Gun Right 9 - Shoot Gun Down 10 - Shoot Gun Left 11 - Throw Grenade Up 12 -
			 * Throw Grenade Right 13 - Throw Grenade Down 14 - Throw Grenade Left
			 */
			if (state.getStateValue() > -200) {

				for (int moveIndex = 0; moveIndex < 15; moveIndex++) {

					int nextPlayer = playerIndex + 1 > state.getPlayerCount() - 1 ? 0 : playerIndex + 1;
					double score = minimaxAB(depth + 1, maxDepth, nextPlayer,
							new GameState(state, this.playerIndex, moveIndex), alpha, beta);

					if (state.players[playerIndex].getIllegalMove() < 0)
						state.players[playerIndex].toggleIllegalMove();

					if (score > -200) {
						if (rnd.nextInt(100) == 10)
							score = 100;
					}
					
					maxScore = Math.max(maxScore, score);
					
					if (maxScore != -10)
						alpha = Math.max(maxScore, alpha);

					if (beta <= alpha)
						break;

					if (depth == 0) {
						if (maxScore > oldScore) {
							this.turn = moveIndex;
							//Log.printConsole("MINIMAX", "playerIndex: " + playerIndex + " | oldScore: " + oldScore
							//		+ " | maxScore: " + maxScore + " | Modified turn: " + this.turn);
							oldScore = maxScore;
						}
					}
				}
			}

		} else {
			if (state.getStateValue() > -200) {
				for (int moveIndex = 0; moveIndex < 15; moveIndex++) {

					int nextPlayer = playerIndex + 1 > state.getPlayerCount() - 1 ? 0 : playerIndex + 1;
					double score = minimaxAB(depth + 1, maxDepth, nextPlayer,
							new GameState(state, this.playerIndex, moveIndex), alpha, beta);

					if (state.players[playerIndex].getIllegalMove() < 0)
						state.players[playerIndex].toggleIllegalMove();

					if (score > -200) {
						if (rnd.nextInt(100) == 10)
							score = -100;
					}

					if (maxScore == -10 && score > -200)
						maxScore = score;
					else if (score > -200)
						maxScore = Math.min(maxScore, score);

					if (maxScore != -10)
						beta = Math.min(maxScore, beta);

					if (beta <= alpha)
						break;

					if (depth == 0) {
						if (maxScore < oldScore) {
							this.turn = moveIndex;
							//Log.printConsole("MINIMAX", "playerIndex: " + playerIndex + " | oldScore: " + oldScore
							//		+ " | maxScore: " + maxScore + " | Modified turn: " + this.turn);
							oldScore = maxScore;
						}
					}
				}
			}
		}
		return maxScore;
	}
}
