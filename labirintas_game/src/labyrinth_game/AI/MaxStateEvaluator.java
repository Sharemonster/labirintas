package labyrinth_game.AI;

import labyrinth_game.Game.Player;
import labyrinth_game.Logging.Log;

public class MaxStateEvaluator implements GameStateEvaluator {

	@Override
	public double calculateGrenadeValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getWeapons()[2]);
	}

	@Override
	public double calculateKnifeValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getWeapons()[0]);
	}

	@Override
	public double calculateGunValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getWeapons()[1]);
	}

	@Override
	public double calculateTreasureValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getTreasure());
	}

	@Override
	public double calculateSelfLivesValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getLives());
	}

	@Override
	public double calculateComparedLivesValue(int playerIndex, Player[] players) {
		return (players[playerIndex].getLives()
				/ (getMax(playerIndex, players, 4) > 0 ? getMax(playerIndex, players, 4) : 1));
	}

	private int getMax(int playerIndex, Player[] players, int value) {
		int max = 0;
		for (int i = 0; i < players.length; i++) {
			if (i != playerIndex) {
				switch (value) {
				case 0: { // Peilis
					if (players[i].getWeapons()[0] > max)
						max = players[i].getWeapons()[0];
					break;
				}
				case 1: { // Kulka
					if (players[i].getWeapons()[1] > max)
						max = players[i].getWeapons()[1];
					break;
				}
				case 2: { // Granata
					if (players[i].getWeapons()[2] > max)
						max = players[i].getWeapons()[2];
					break;
				}
				case 3: { // Lobis
					if (players[i].getTreasure() > max)
						max = players[i].getTreasure();
					break;
				}
				case 4: { // Gyvybes
					if (players[i].getLives() > max)
						max = players[i].getLives();
					break;
				}
				}
			}
		}
		return max < 0 ? 0 : max;
	}

	@Override
	public double calculateIllegalMove(int playerIndex, Player[] players) {
		return players[playerIndex].getIllegalMove();
	}

	public double calculateWalls(int oldWalls, int newWalls) {
		return oldWalls - newWalls;
	}

	public double calculateMovement(int playerIndex, Player[] players, int oldX, int oldY) {
		return Math.abs(players[playerIndex].getX() - oldX) + Math.abs(players[playerIndex].getY() - oldY);
	}

}
