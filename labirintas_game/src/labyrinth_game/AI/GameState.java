package labyrinth_game.AI;

import java.util.ArrayList;

import labyrinth_game.Cell.*;
import labyrinth_game.Control.Fighting;
import labyrinth_game.Control.Movement;
import labyrinth_game.Game.GameMap;
import labyrinth_game.Game.Player;
import labyrinth_game.Logging.*;

public class GameState {

	int[] movementVector = new int[] { 0, -1, 1, 0, 0, 1, -1, 0 };

	Cell[][] gameMap;
	Player[] players;

	int walls = 0, oldX, oldY;

	GameState previousState;
	GameStateEvaluator stateEvaluator = new MaxStateEvaluator();

	double stateValue;

	public GameState(GameMap labyrinth, int playerIndex, ArrayList<Player> players) {

		this.previousState = null;

		cloneMap(labyrinth);
		clonePlayers(players);

		oldX = -1;
		oldY = -1;

		walls = calculateWalls();
		stateValue = evaluateState(playerIndex);
	}

	public GameState(GameState previousState, int playerIndex, int moveIndex) {
		this.previousState = previousState;

		cloneMapCell(previousState.gameMap);
		clonePlayersArray(previousState.players);

		oldX = players[playerIndex].getX();
		oldY = players[playerIndex].getY();

		doMove(moveIndex, playerIndex);

		walls = calculateWalls();
		stateValue = evaluateState(playerIndex);

	}

	public double getStateValue() {
		return this.stateValue;
	}

	private double evaluateState(int playerIndex) {
		double wGrenade = 1.0, wKnife = 1.0, wGun = 1.0, wGoodTreasure = 10.0, wSelfLives = 1.0, wComparedLives = 1.0,
				wIllegalMove = 100.0, wWalls = 1.0, wMovement = 2.0;

		/*
		 * Log.printConsole("GAMESTATE", "\nGrenade: " + wGrenade + "*" +
		 * stateEvaluator.calculateGrenadeValue(playerIndex, players) + "\nKnife: " +
		 * wKnife + "*" + stateEvaluator.calculateKnifeValue(playerIndex, players) +
		 * "\nGun: " + wGun + "*" + stateEvaluator.calculateGunValue(playerIndex,
		 * players) + "\nTreasure: " + wGoodTreasure + "*" +
		 * stateEvaluator.calculateTreasureValue(playerIndex, players) + "\nLives: " +
		 * wSelfLives + "*" + stateEvaluator.calculateSelfLivesValue(playerIndex,
		 * players) + "\nCompared Lives: " + wComparedLives + "*" +
		 * stateEvaluator.calculateComparedLivesValue(playerIndex, players) +
		 * "\nIllegal Move: " + wIllegalMove + "*" +
		 * stateEvaluator.calculateIllegalMove(playerIndex, players) + "\nWalls: " +
		 * wWalls + "*" + stateEvaluator.calculateWalls( previousState == null ?
		 * getWallCount() : previousState.getWallCount(), getWallCount()));
		 */

		return (wGrenade * stateEvaluator.calculateGrenadeValue(playerIndex, players)
				+ wKnife * stateEvaluator.calculateKnifeValue(playerIndex, players)
				+ wGun * stateEvaluator.calculateGunValue(playerIndex, players)
				+ wGoodTreasure * stateEvaluator.calculateTreasureValue(playerIndex, players)
				+ wSelfLives * stateEvaluator.calculateSelfLivesValue(playerIndex, players)
				+ wComparedLives * stateEvaluator.calculateComparedLivesValue(playerIndex, players)
				+ wIllegalMove * stateEvaluator.calculateIllegalMove(playerIndex, players)
				+ wWalls * stateEvaluator.calculateWalls(
						previousState == null ? getWallCount() : previousState.getWallCount(), getWallCount())
				+ wMovement * stateEvaluator.calculateMovement(playerIndex, players,
						oldX != -1 ? oldX : players[playerIndex].getX(),
						oldY != -1 ? oldY : players[playerIndex].getY()));
	}

	public int getWallCount() {
		return this.walls;
	}

	private int calculateWalls() {
		int wallCount = 0;
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				for (int w = 0; w < 4; w++) {
					if (gameMap[i][j].getWall(w) > 0) {
						wallCount++;
					}
				}
			}
		}
		return wallCount;
	}

	private void cloneMap(GameMap labyrinth) {
		this.gameMap = new Cell[labyrinth.getMapSize()][labyrinth.getMapSize()];

		for (int i = 0; i < this.gameMap.length; i++) {
			for (int j = 0; j < this.gameMap.length; j++) {

				switch (labyrinth.getGameMap()[i][j].getClass().getSimpleName()) {
				case "River": {
					this.gameMap[i][j] = new River(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "Hole": {
					this.gameMap[i][j] = new Hole(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "Land": {
					this.gameMap[i][j] = new Land(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					copyTreasure(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "Armory": {
					this.gameMap[i][j] = new Armory(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "Infirmary": {
					this.gameMap[i][j] = new Infirmary(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "RiverMouth": {
					this.gameMap[i][j] = new RiverMouth(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				case "Swamp": {
					this.gameMap[i][j] = new Swamp(i, j);
					copyWalls(labyrinth.getGameMap()[i][j], i, j);
					break;
				}
				}
			}
		}

		for (int i = 0; i < this.gameMap.length; i++) {
			for (int j = 0; j < this.gameMap.length; j++) {
				copyLinks(labyrinth.getGameMap()[i][j], i, j);
			}
		}
	}

	private void cloneMapCell(Cell[][] gameMap) {
		this.gameMap = new Cell[gameMap.length][gameMap[0].length];

		for (int i = 0; i < this.gameMap.length; i++) {
			for (int j = 0; j < this.gameMap.length; j++) {

				switch (gameMap[i][j].getClass().getSimpleName()) {
				case "River": {
					this.gameMap[i][j] = new River(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				case "Hole": {
					this.gameMap[i][j] = new Hole(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				case "Land": {
					this.gameMap[i][j] = new Land(i, j);
					copyWalls(gameMap[i][j], i, j);
					copyTreasure(gameMap[i][j], i, j);
					break;
				}
				case "Armory": {
					this.gameMap[i][j] = new Armory(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				case "Infirmary": {
					this.gameMap[i][j] = new Infirmary(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				case "RiverMouth": {
					this.gameMap[i][j] = new RiverMouth(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				case "Swamp": {
					this.gameMap[i][j] = new Swamp(i, j);
					copyWalls(gameMap[i][j], i, j);
					break;
				}
				}
			}
		}

		for (int i = 0; i < this.gameMap.length; i++) {
			for (int j = 0; j < this.gameMap.length; j++) {
				copyLinks(gameMap[i][j], i, j);
			}
		}
	}

	private void copyWalls(Cell source, int x, int y) {
		for (int i = 0; i < 4; i++)
			this.gameMap[x][y].setWall(i, source.getWall(i));
	}

	private void copyLinks(Cell source, int x, int y) {
		try {
			((Teleportable) this.gameMap[x][y]).setPreviousCell(
					this.gameMap[((Teleportable) source).getPreviousCell().getX()][((Teleportable) source)
							.getPreviousCell().getY()]);
			((Teleportable) this.gameMap[x][y])
					.setNextCell(this.gameMap[((Teleportable) source).getNextCell().getX()][((Teleportable) source)
							.getNextCell().getY()]);
		} catch (Exception e) {
			// Log.printFile("GAMESTATE", e.getMessage() + " |||| " +
			// e.getClass().getSimpleName(), 4);
		}
	}

	private void copyTreasure(Cell source, int x, int y) {
		try {
			((Lootable) this.gameMap[x][y]).setTreasure(((Lootable) source).getTreasure());
		} catch (Exception e) {
			// Log.printFile("GAMESTATE", e.getMessage() + " |||| " +
			// e.getClass().getSimpleName(), 4);
		}
	}

	private void clonePlayers(ArrayList<Player> source) {
		this.players = new Player[source.size()];

		for (int i = 0; i < source.size(); i++) {
			players[i] = (Player) source.get(i).clone();
		}
	}

	private void clonePlayersArray(Player[] source) {
		this.players = new Player[source.length];

		for (int i = 0; i < source.length; i++) {
			players[i] = (Player) source[i].clone();
		}
	}

	public int getPlayerCount() {
		return players.length;
	}

	/*
	 * Legitimate moves: 0 - Move Up 1 - Move Right 2 - Move Down 3 - Move Left 4 -
	 * Pick Up Treasure 5 - Drop Treasure 6 - Use Knife 7 - Shoot Gun Up 8 - Shoot
	 * Gun Right 9 - Shoot Gun Down 10 - Shoot Gun Left 11 - Throw Grenade Up 12 -
	 * Throw Grenade Right 13 - Throw Grenade Down 14 - Throw Grenade Left
	 */
	private void doMove(int moveIndex, int playerIndex) {
		Player movingPlayer;
		int result = 0;

		movingPlayer = players[playerIndex];

		switch (moveIndex) {
		case 0:
		case 1:
		case 2:
		case 3: {
			result = Movement.move(gameMap, movingPlayer, moveIndex, new ArrayList<String>());

			if (result > 0)
				Movement.executeCell(gameMap, movingPlayer, new ArrayList<String>());
			break;
		}
		case 4: {
			try {
				if (((Lootable) gameMap[movingPlayer.getX()][movingPlayer.getY()]).getTreasure() > 0) {
					movingPlayer
							.setTreasure(((Lootable) gameMap[movingPlayer.getX()][movingPlayer.getY()]).getTreasure());
					((Lootable) gameMap[movingPlayer.getX()][movingPlayer.getY()]).setTreasure(0);
					result = 1;
				} else
					result = 0;
			} catch (ClassCastException cce) {
				//Log.printFile("GAMESTATE", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
				//Log.printConsole("GAMESTATE", e.getMessage() + " |||| " + e.getClass().getSimpleName());
			}catch (Exception e) {
				Log.printFile("GAMESTATE", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
				Log.printConsole("GAMESTATE", e.getMessage() + " |||| " + e.getClass().getSimpleName());
			}
			break;
		}
		case 5: {
			try {
				if (gameMap[movingPlayer.getX()][movingPlayer.getY()].getClass().getSimpleName().contentEquals("Land")
						&& movingPlayer.getTreasure() > 0) {

					((Lootable) gameMap[movingPlayer.getX()][movingPlayer.getY()])
							.setTreasure(movingPlayer.getTreasure());
					movingPlayer.setTreasure(0);
					result = 1;

				} else
					result = 0;
			} catch (Exception e) {
				// Log.printFile("GAMESTATE", e.getMessage() + " |||| " +
				// e.getClass().getSimpleName(), 4);
			}
			break;

		}
		case 6: {
			result = Fighting.useWeapon(gameMap, movingPlayer, players, 0, moveIndex, new ArrayList<String>());
			break;
		}
		case 7:
		case 8:
		case 9:
		case 10: {
			result = Fighting.useWeapon(gameMap, movingPlayer, players, 1, moveIndex - 7, new ArrayList<String>());
			break;
		}
		case 11:
		case 12:
		case 13:
		case 14: {
			result = Fighting.useWeapon(gameMap, movingPlayer, players, 2, moveIndex - 11, new ArrayList<String>());
			break;
		}
		}
		if (result == 0) {
			if (movingPlayer.getIllegalMove() > -1)
				movingPlayer.toggleIllegalMove();
		}
	}

	private String printGameMap() {
		String resultString = "";

		String line = "_____________________________________________________________\n";

		resultString += line;
		// resultString = writeHoleList(resultString) + line;
		resultString = writeWalls(resultString) + line + "\n";
		// resultString = writeWallList(resultString) + line;
		// resultString = writeCellList(resultString, line) + line;

		return resultString;
	}

	private String writeCellList(String resultString, String line) {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				resultString += gameMap[j][i].toString();
				resultString += "\n";
				resultString += line;
			}
		}
		return resultString;
	}

	private String writeWallList(String resultString) {
		for (int i = 0; i < gameMap.length; i++) {
			resultString += "||  ";
			for (int j = 0; j < gameMap.length; j++) {
				for (int k = 0; k < 4; k++)
					resultString += gameMap[j][i].getWall(k);
				resultString += "  ||  ";
			}
			resultString += "\n";
		}
		return resultString;
	}

	private String writeHoleList(String resultString) {
		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {
				if (isClass(i, j, new String[] { "Hole" })) {
					try {
						resultString += gameMap[i][j].getX() + " " + gameMap[i][j].getY() + " --> "
								+ ((Hole) gameMap[i][j]).getNextCell().getX() + " "
								+ ((Hole) gameMap[i][j]).getNextCell().getY() + "\n";
					} catch (NullPointerException e) {
						System.out.println(e.getMessage());
						System.out.println("X: " + i + " Y: " + j);
						resultString += (gameMap[i][j].getX() + " " + gameMap[i][j].getY() + " -->  ???\n");
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}
		}
		return resultString;
	}

	private String writeWalls(String resultString) {
		String verticalWall = "+";
		String horizontalWall = "++++++++++++";
		String verticalPath = " ";
		String horizontalPath = "            ";
		String verticalExit = "|";
		String horizontalExit = "------------";

		resultString = writeTopWall(resultString, horizontalWall, horizontalPath, horizontalExit) + "\n";

		for (int i = 0; i < gameMap.length; i++) {
			for (int j = 0; j < gameMap[i].length; j++) {

				if (gameMap[j][i].getWall(3) == 1 || gameMap[j][i].getWall(3) == 2) {
					resultString += String.format("%s", verticalWall);
				} else if (gameMap[j][i].getWall(3) == 3) {
					resultString += String.format("%s", verticalExit);
				} else {
					resultString += String.format("%s", verticalPath);
				}

				resultString += String.format("%11s", gameMap[j][i].getName());

			}
			if (gameMap[gameMap.length - 1][i].getWall(1) == 1 || gameMap[gameMap.length - 1][i].getWall(1) == 2) {
				resultString += String.format("%s", verticalWall);
			} else if (gameMap[gameMap.length - 1][i].getWall(1) == 3) {
				resultString += String.format("%s", verticalExit);
			} else {
				resultString += String.format("%s", verticalPath);
			}
			resultString += "\n";

			for (int j = 0; j < gameMap.length; j++) {
				if (gameMap[j][i].getWall(2) == 1 || gameMap[j][i].getWall(2) == 2) {
					resultString += String.format("%s", horizontalWall);
				} else if (gameMap[j][i].getWall(2) == 3) {
					resultString += String.format("%s", horizontalExit);
				} else {
					resultString += String.format("%s", horizontalPath);
				}
			}

			resultString += "\n";
		}
		return resultString;
	}

	private String writeTopWall(String resultString, String horizontalWall, String horizontalPath,
			String horizontalExit) {
		for (int j = 0; j < gameMap.length; j++) {
			if (gameMap[j][0].getWall(0) == 1 || gameMap[j][0].getWall(0) == 2) {
				resultString += String.format("%s", horizontalWall);
			} else if (gameMap[j][0].getWall(0) == 3) {
				resultString += String.format("%s", horizontalExit);
			} else {
				resultString += String.format("%s", horizontalPath);
			}
		}
		return resultString;
	}

	public Cell[][] getGameMap() {
		return gameMap;
	}

	public boolean isClass(int i, int j, String[] classNames) {
		boolean isClass = false;
		for (String className : classNames) {
			if (gameMap[i][j].getClass().getSimpleName().equals(className))
				isClass = true;
		}
		return isClass;
	}

}
