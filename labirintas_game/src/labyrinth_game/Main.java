package labyrinth_game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import com.sun.xml.internal.ws.api.message.Messages;

import labyrinth_game.AI.GameStateEvaluator;
import labyrinth_game.AI.MaxStateEvaluator;
import labyrinth_game.Control.Movement;
import labyrinth_game.Game.ComputerPlayer;
import labyrinth_game.Game.GameMap;
import labyrinth_game.Game.HumanPlayer;
import labyrinth_game.Game.MoveResult;
import labyrinth_game.Game.Player;
import labyrinth_game.Logging.Log;

public class Main {

	public static void main(String[] args) {
		GameMap gameMap = new GameMap(5);
		Controller control = new Controller(gameMap);
		Random rng = new Random();
		ArrayList<Player> players = new ArrayList<Player>();
		Scanner sc = new Scanner(System.in);

		System.out.println("Kiek tikru zaideju zais?");
		String ats = sc.nextLine();
		try {
			for (int i = 0; i < Integer.parseInt(ats); i++) {
				players.add(new HumanPlayer());
				System.out.println("Koks " + (i + 1) + " zaidejo vardas?");
				players.get(i).setName(sc.nextLine());
				players.get(i).setX(rng.nextInt(5));
				players.get(i).setY(rng.nextInt(5));
			}
		} catch (Exception e) {
			System.out.println("Ivyko klaida\n Programa issijungs");
			Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
			System.exit(0);
		}

		System.out.println("Kiek dirbtiniu zaideju zais?");
		ats = sc.nextLine();
		int size = players.size();
		try {
			for (int i = 0; i < Integer.parseInt(ats); i++) {
				players.add(new ComputerPlayer());
				players.get(i + size).setName("AI " + (i + 1));
				players.get(i + size).setX(rng.nextInt(5));
				players.get(i + size).setY(rng.nextInt(5));
			}
		} catch (Exception e) {
			System.out.println("Ivyko klaida\n Programa issijungs");
			Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
			System.exit(0);
		}
		// sc.close();

		int playerIndex = 0;
		String in = "";
		Player playing;

		// Pradines zaideju pozicijos
		{
			for (int i = 0; i < players.size(); i++) {
				System.out.println(
						players.get(i).getName() + "\nX: " + players.get(i).getX() + " Y: " + players.get(i).getY());
				ArrayList<String> messages = new ArrayList<String>();
				Movement.executeCell(gameMap.getGameMap(), players.get(i), messages);
				for (String a : messages) {
					System.out.println(a);
				}
				messages.clear();
			}
		}

		try {
			for (int i = 0; i < players.size(); i++) {
				((ComputerPlayer) players.get(i)).setGameState(new labyrinth_game.AI.GameState(gameMap, i, players));
			}
		} catch (Exception e) {
			Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
		}

		// Nustatomas pradinis zaidejas
		playing = players.get(0);
		printRules();
		do {
			try {
				for (int i = 0; i < players.size(); i++) {
					((ComputerPlayer) players.get(i))
							.setGameState(new labyrinth_game.AI.GameState(gameMap, i, players));
				}
			} catch (Exception e) {
				Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
			}

			System.out.println(playing.getName() + " ejimas");
			in = playing.getMove(playerIndex);
			String[] moveString = in.split(" ");

			try {
				System.out.println(resolveMove(moveString));

				Player[] array = new Player[players.size()];
				for (int i = 0; i < array.length; i++)
					array[i] = players.get(i);

				MoveResult moveResult = control.executeMove(Integer.parseInt(moveString[0]),
						Integer.parseInt(moveString[1]), playing, array);
				if (!playing.getExited()) {
					ArrayList<String> s = new ArrayList<String>();
					Movement.executeCell(gameMap.getGameMap(), playing, s);
					for (String a : s) {
						moveResult.appendMessage(a);
					}

					System.out.println(moveResult.getMessage());
				}

				if (moveResult.getResult() != 0) {
					do {
						players.get(playerIndex).setMovementPenalty(players.get(playerIndex).getMovementPenalty() - 1);
						if (playerIndex + 1 < players.size())
							playerIndex++;
						else
							playerIndex = 0;
					} while (players.get(playerIndex).getMovementPenalty() > 0);
					playing = players.get(playerIndex);
				}

			} catch (Exception e) {
				Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
				System.out.println(e.getMessage() + "Klaida");
			}

		} while (!in.equals("x"));

	}

	private static String resolveMove(String[] s) {
		// TODO Auto-generated method stub
		String result = "";
		try {
			switch (s[0]) {
			case "0":
			case "1":
			case "2":
			case "3": {
				result = resolveDirection(s[0]);
				break;
			}
			case "4": {
				result = "Paimti lobi";
				break;
			}
			case "5": {
				result = "Mesti lobi";
				break;
			}
			case "6": {
				result = "Peilis";
				break;
			}
			case "7":
			case "8":
			case "9":
			case "10": {
				result = "Sauti " + resolveDirection(s[1]);
				break;
			}
			case "11":
			case "12":
			case "13":
			case "14": {
				result = "Granata " + resolveDirection(s[1]);
				break;
			}
			}
		} catch (Exception e) {
			Log.printFile("MAIN", e.getMessage() + " |||| " + e.getClass().getSimpleName(), 4);
		}
		return result;
	}

	private static String resolveDirection(String s) {
		String result = "";
		switch (s) {
		case "0": {
			result += "Aukstyn";
			break;
		}
		case "1": {
			result += "Desinen";
			break;
		}
		case "2": {
			result += "Zemyn";
			break;
		}
		case "3": {
			result += "Kairen";
			break;
		}
		}
		return result;
	}

	private static void printRules() {
		System.out.println(
				"0 - eiti aukstyn\n1 - eiti desinen\n2 - eiti zemyn\n3 - eiti kairen\n4 - paimti lobi\n5 - ismesti lobi\n6 - durti peiliu\n7 0 - sauti aukstyn\n8 1 - sauti desinen\n9 2 - sauti zemyn\n10 3 - sauti kairen\n11 0 - mesti granata aukstyn\n12 1 - mesti granata desinen\n13 2 - mesti granata zemyn\n14 3 - mesti granata kairen");
	}

}
