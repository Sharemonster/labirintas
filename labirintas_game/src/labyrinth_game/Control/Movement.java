package labyrinth_game.Control;

import java.util.ArrayList;

import labyrinth_game.Cell.Armory;
import labyrinth_game.Cell.Cell;
import labyrinth_game.Cell.Infirmary;
import labyrinth_game.Cell.Lootable;
import labyrinth_game.Cell.Teleportable;
import labyrinth_game.Game.Player;
import labyrinth_game.Logging.Log;

public class Movement {
	static int[] movementVector = new int[] { 0, -1, 1, 0, 0, 1, -1, 0 };

	// Returns result: 0 - failure, 1 - success, 2 - game over
	public static int move(Cell[][] gameMap, Player player, int direction, ArrayList<String> messages) {

		if (!player.getExited()) {
			int u = player.getX() + movementVector[2 * direction],
					v = player.getY() + movementVector[2 * direction + 1];
			if (withinBounds(u, v, gameMap.length)) {
				if (gameMap[player.getX()][player.getY()].getWall(direction) != 0) {
					messages.add("Negalima\n");
					return 0;
				} else {
					player.setX(u);
					player.setY(v);
					return 1;
				}
			} else {
				if (gameMap[player.getX()][player.getY()].getWall(direction) == 3) {
					player.setExited(true);
					player.setExitDirection(direction);
					player.setX(u);
					player.setY(v);
					messages.add("Radote isejima is labirinto!\n");

					switch (player.getTreasure()) {
					case 0: {
						messages.add("Gaila neturi lobio\n");
						return 1;
					}
					case 1: {
						messages.add("Gaila tavo lobis netikras");
						return 1;
					}
					case 2: {
						messages.add("Tavo lobis tikras! Tu laimejai\n");
						return 2;
					}
					}

				} else {
					messages.add("Negalima\n");
					return 0;
				}
			}
		} else {
			if (direction != (player.getExitDirection() > 2 ? player.getExitDirection() - 2
					: player.getExitDirection() + 2)) {
				messages.add("Negalima\n");
				return 0;
			}
		}
		return 0;
	}

	private static boolean withinBounds(int x, int y, int size) {
		if (x > 0 || x < size || y > 0 || y < size)
			return true;
		else
			return false;
	}

	public static int executeCell(Cell[][] gameMap, Player player, ArrayList<String> messages) {
		int x = player.getX(), y = player.getY();
		switch (gameMap[x][y].getClass().getSimpleName()) {
		case "River": {
			for (int i = 0; i < 3; i++) {
				try {
					int temporary = ((Teleportable) gameMap[x][y]).getNextCell().getX();
					//Log.printFile("MOVEMENT execute", "Next cell: " + ((Teleportable) gameMap[x][y]).getNextCell(), 10);
					y = ((Teleportable) gameMap[x][y]).getNextCell().getY();
					x = temporary;
				} catch (Exception e) {
					Log.printFile("MOVEMENT", e.getMessage(), 0);
				}
			}
			player.setX(x);
			player.setY(y);
			messages.add(gameMap[x][y].getName() + "\n");
			return 1;
		}
		case "Hole": {
			int temporary = ((Teleportable) gameMap[x][y]).getNextCell().getX();
			//Log.printFile("MOVEMENT execute", "Next cell: " + ((Teleportable) gameMap[x][y]).getNextCell(), 10);
			y = ((Teleportable) gameMap[x][y]).getNextCell().getY();
			x = temporary;
			player.setX(x);
			player.setY(y);
			messages.add(gameMap[x][y].getName() + "\n");
			return 1;
		}
		case "Infirmary": {
			((Infirmary) gameMap[x][y]).healPlayer(player);
			player.setX(x);
			player.setY(y);
			messages.add(gameMap[x][y].getName() + "\n");
			messages.add("Tu pasigydei\n");
			return 1;
		}
		case "Armory": {
			((Armory) gameMap[x][y]).equipWeapons(player);
			player.setX(x);
			player.setY(y);
			messages.add(gameMap[x][y].getName() + "\n");
			messages.add("Tavo amunicija papildyta\n");
			return 1;
		}
		case "Land": {
			messages.add(gameMap[x][y].getName() + "\n");
			if (((Lootable) gameMap[x][y]).getTreasure() == 1 || ((Lootable) gameMap[x][y]).getTreasure() == 2) {
				messages.add("Lobis!\n");
			}
			return 1;
		}
		case "Swamp": {

		}
		case "RiverMouth": {
			messages.add(gameMap[x][y].getName() + "\n");
			return 1;
		}
		}
		return 0;
	}
}
