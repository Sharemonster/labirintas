package labyrinth_game.Control;

import java.util.ArrayList;

import labyrinth_game.Cell.Cell;
import labyrinth_game.Game.Player;

public class Fighting {

	// Returns result: 0 - failure, 1 - success, 2 - game over
	static int[] movementVector = new int[] { 0, -1, 1, 0, 0, 1, -1, 0 };

	public static int useWeapon(Cell[][] gameMap, Player playing, Player[] players, int weaponIndex, int direction,
			ArrayList<String> messages) {
		boolean hit = false;

		if (playing.getWeapons()[weaponIndex] > 0) {
			switch (weaponIndex) {
			case 0: {
				for (int i = 0; i < players.length; i++) {
					if (!players[i].equals(playing)) {
						if (players[i].getX() == playing.getX() && players[i].getY() == playing.getY()) {
							players[i].injure();
							for (int j = 0; j < 3; j++)
								playing.addWeapon(players[i].getWeapons()[j], j);
							players[i].transferWeapon(new int[] { 0, 0, 0 });
							playing.addWeapon(-1, 0);
							hit = true;
							messages.add(players[i].getName() + " suzeistas\n");
							messages.add(players[i].getName() + " neteko ginklu\n");
							if (players[i].getLives() <= 0) {
								players[i].setDead(true);
								messages.add(players[i].getName() + " mire\n");
							}
						}
					}
				}
				if (!hit) {
					playing.injure();
					playing.addWeapon(-1, 0);
					messages.add("Pataikei i save\n");
					if (playing.getLives() == 0) {
						playing.setDead(true);
						messages.add(playing.getName() + " mire\n");
					}
				}
				return 1;
			}
			case 1: {
				if (direction == 1 || direction == 3) {
					for (int i = playing.getX(); i < gameMap.length && i >= 0; i += movementVector[2 * direction]) {
						for (int j = 0; j < players.length; j++) {
							if (!players[j].equals(playing)) {
								if (players[j].getX() == i && players[j].getY() == playing.getY()) {
									players[j].injure();
									if (players[j].getLives() <= 0) {
										players[j].setDead(true);
										messages.add(players[j].getName() + " mire\n");
									} else
										messages.add(players[j].getName() + " suzeistas\n");
								}
							}
						}
					}
				} else {
					for (int i = playing.getY(); i < gameMap.length && i >= 0; i += movementVector[2 * direction + 1]) {
						for (int j = 0; j < players.length; j++) {
							if (!players[j].equals(playing)) {
								if (players[j].getY() == i && players[j].getX() == playing.getX()) {
									players[j].injure();

									if (players[j].getLives() == 0) {
										players[j].setDead(true);
										messages.add(players[j].getName() + " mire\n");
									} else
										messages.add(players[j].getName() + " suzeistas\n");
								}
							}
						}
					}
				}
				if (gameMap[playing.getX()][playing.getY()].getWall(direction) == 2) {
					messages.add("Kulka atsimuse i siena\n");
				} else {
					messages.add("Kulka pramuse siena\n");
				}
				playing.addWeapon(-1, 1);
				return 1;
			}
			case 2: {
				switch (gameMap[playing.getX()][playing.getY()].getWall(direction)) {
				case 0: {
					playing.addWeapon(-1, 2);
					messages.add("Veltui...\n");
					return 1;
				}
				case 1: {
					playing.addWeapon(-1, 2);
					gameMap[playing.getX()][playing.getY()].setWall(direction, 0);
					messages.add("Siena susprogo\n");
					return 1;
				}
				case 2: {
					playing.addWeapon(-1, 2);
					messages.add("Siena nesprogo\n");
					return 1;
				}
				}
				break;
			}
			}
		} else {
			switch (weaponIndex) {
			case 0: {
				messages.add("Neturi peilio\n");
				return 0;
			}
			case 1: {
				messages.add("Neturi soviniu\n");
				return 0;
			}
			case 2: {
				messages.add("Neturi granatu\n");
				return 0;
			}
			}
		}

		return 0;
	}

}
