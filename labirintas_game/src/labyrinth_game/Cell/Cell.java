package labyrinth_game.Cell;

public interface Cell {
	
	public String getName();

	public int getAccessDifficulty();

	public void setAccessDifficulty(int accessDifficulty);

	public void setWall(int direction, int wall);

	public int getWall(int direction);

	public int getX();

	public int getY();

	public void setX(int x);

	public void setY(int y);

	public boolean getSpecialAvailability();

	public void setSpecialAvailability(boolean available);

	/*
	 * // Teleportuojanciu langeliu metodai public abstract void
	 * setAnkstesnis(Langelis ankstesnis);
	 * 
	 * public abstract void setSekantis(Langelis sekantis);
	 * 
	 * public abstract Langelis getAnkstesnis();
	 * 
	 * public abstract Langelis getSekantis();
	 * 
	 * public abstract int getUpesNr();
	 * 
	 * public abstract void setUpesNr(int nr);
	 * 
	 * // Langeliu galinciu tureti lobi metodai public abstract void
	 * padetiLobi(Zaidejas zaid, int lobis);
	 * 
	 * public abstract void paimtiLobi(Zaidejas zaid);
	 * 
	 * public abstract void setLobis(int lobis);
	 * 
	 * public abstract int checkLobis();
	 * 
	 * //Medpunkto public abstract void papildytiGyvybes(Zaidejas zaid);
	 * 
	 * //Arsenalo public abstract void papildytiGinklus(Zaidejas zaid);
	 */
}
