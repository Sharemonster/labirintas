package labyrinth_game.Cell;

import java.util.Arrays;

import labyrinth_game.Game.HumanPlayer;

public class Land implements Cell, Lootable {

	String name;
	int x, y; // Cell coordinates

	int accessDifficulty;
	boolean available;
	public int[] wall; // 0 - No wall | 1 - Destructable | 2 - Indestructable
						// [0] - Up | [1] - Right | [2] - Down | [3] - Left
	int treasure;

	public Land(int x, int y) {
		this.name = "Sausuma";

		this.x = x;
		this.y = y;

		wall = new int[] { 1, 1, 1, 1 };

		accessDifficulty = 0;
		available = false;

		treasure = 0;
	}

	public String getName() {
		return this.name;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWall(int direction, int wall) {
		this.wall[direction] = wall;
	}

	public int getWall(int direction) {
		return this.wall[direction];
	}

	public void placeTreasure(HumanPlayer player, int treasure) {

	}

	public void takeTreasure(HumanPlayer player) {

	}

	public void setTreasure(int treasure) {
		this.treasure = treasure;
	}

	public int getTreasure() {
		return this.treasure;
	}

	public int getAccessDifficulty() {
		return accessDifficulty;
	}

	public void setAccessDifficulty(int accessDifficulty) {
		this.accessDifficulty = accessDifficulty;
	}

	public boolean getSpecialAvailability() {
		return available;
	}

	public void setSpecialAvailability(boolean available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return new String("Name: " + name + "\nX: " + x + " Y: " + y + "\nWalls (starting with Up CW): "
				+ Arrays.toString(wall) + "\nTreasure (0 - none, 1 - true treasure, 2 - false treasure): " + treasure);
	}

}
