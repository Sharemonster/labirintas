package labyrinth_game.Cell;

import labyrinth_game.Game.HumanPlayer;

public interface Lootable {
	
	public void placeTreasure(HumanPlayer player, int treasure);
	public void takeTreasure(HumanPlayer player);
	public void setTreasure(int treasure);
	public int getTreasure();
}
