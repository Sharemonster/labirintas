package labyrinth_game.Cell;

public interface Teleportable {

	public void setPreviousCell(Cell previousCell);

	public void setNextCell(Cell nextCell);

	public Cell getPreviousCell();

	public Cell getNextCell();
	
	public int getRiverNumber();

	public void setRiverNumber(int riverNumber);

}
