package labyrinth_game.Cell;

import java.util.Arrays;

import labyrinth_game.Logging.Log;

public class Swamp implements Cell, Teleportable {

	String name;
	int x, y; // Cell coordinates

	int accessDifficulty = 0;
	boolean available = false;
	public int[] wall; // 0 - No wall | 1 - Destructable | 2 - Indestructable
						// [0] - Up | [1] - Right | [2] - Down | [3] - Left

	Cell nextCell;
	int riverNumber;

	public Swamp(int x, int y) {
		this.name = "Pelke";

		this.x = x;
		this.y = y;

		wall = new int[] { 1, 1, 1, 1 };

		accessDifficulty = 0;
		available = false;

		nextCell = null;
		riverNumber = -1;
	}

	public String getName() {
		return this.name;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWall(int direction, int wall) {
		this.wall[direction] = wall;
	}

	public int getWall(int direction) {
		return this.wall[direction];
	}

	@Override
	public int getAccessDifficulty() {
		return accessDifficulty;
	}

	@Override
	public void setAccessDifficulty(int accessDifficulty) {
		this.accessDifficulty = accessDifficulty;
	}

	@Override
	public boolean getSpecialAvailability() {
		return available;
	}

	@Override
	public void setSpecialAvailability(boolean available) {
		this.available = available;
	}

	@Override
	public void setPreviousCell(Cell previousCell) {
		//Log.printConsole("CELL", "Swamp doesn't have a previous cell");
	}

	@Override
	public void setNextCell(Cell nextCell) {
		this.nextCell = nextCell;
	}

	@Override
	public Cell getPreviousCell() {
		//Log.printConsole("CELL", "Swamp doesn't have a previous cell");
		return null;
	}

	@Override
	public Cell getNextCell() {
		return nextCell;
	}

	@Override
	public int getRiverNumber() {
		return riverNumber;
	}

	@Override
	public void setRiverNumber(int riverNumber) {
		this.riverNumber = riverNumber;
	}

	@Override
	public String toString() {
		return new String(
				"Name: " + name + "\nX: " + x + " Y: " + y + "\nWalls (starting with Up CW): " + Arrays.toString(wall)
						+ "\nRiver Number: " + riverNumber);
	}

}
