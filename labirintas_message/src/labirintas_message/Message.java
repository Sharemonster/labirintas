package labirintas_message;

import java.io.Serializable;
import java.util.ArrayList;

public class Message implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8778805813219212234L;

	public static final int LOGIN = 0, SYSTEM_MSG = 1, GAME_MSG = 2, LOGOUT = 3;

	int tipas, komandos_tipas;
	String msg, username, password;
	ArrayList<Skelbimas> skelbimai;

	public Message(int tipas, int komandos_tipas, String msg, ArrayList<Skelbimas> skelbimai) {
		this.tipas = tipas;
		this.komandos_tipas = komandos_tipas;
		this.msg = msg;
		this.skelbimai = skelbimai;
	}

	public Message(String username, String password) {
		this.tipas = 0;
		this.username = username;
		this.password = password;
	}

	public int getTipas() {
		return tipas;
	}
	
	public int getKomandosTipas() {
		return komandos_tipas;
	}

	public String getMsg() {
		return msg;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	public ArrayList<Skelbimas> getSkelbimai(){
		return skelbimai;
	}
}
