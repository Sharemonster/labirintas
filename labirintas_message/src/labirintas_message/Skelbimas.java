package labirintas_message;

import java.io.Serializable;

public class Skelbimas implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6324156108458967449L;
	String gameID;
	int dydis, esamuzaidejuskaicius, maxzaidejuskaicius;
	
	public Skelbimas(String gameID, int dydis, int esamuzaidejuskaicius, int maxzaidejuskaicius) {
		this.gameID = gameID;
		this.dydis = dydis;
		this.esamuzaidejuskaicius = esamuzaidejuskaicius;
		this.maxzaidejuskaicius = maxzaidejuskaicius;
	}
	
	public String getGameID() {
		return gameID;
	}
	public int getDydis() {
		return dydis;
	}
	public int getZaidejuSkaicius() {
		return esamuzaidejuskaicius;
	}
	public int getMaxZaidejuSkaicius() {
		return maxzaidejuskaicius;
	}
	public void setZaidejuSkaicius(int zaidejuskaicius) {
		this.esamuzaidejuskaicius = zaidejuskaicius;
	}
}
